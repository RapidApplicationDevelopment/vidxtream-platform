//
//  NativeVideoPlayer.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#include "../NativeVideoPlayer.h"
#import "native_movie_detail/MovieViewController.h"
#import "cocos2d.h"


namespace
{
	UIViewController* getRootViewController()
	{
		UIViewController *result = nil;
		
			// Try to find the root view controller programmically
		
			// Find the top window (that is not an alert view or other window)
		UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
		if (topWindow.windowLevel != UIWindowLevelNormal)
		{
			NSArray *windows = [[UIApplication sharedApplication] windows];
			for(topWindow in windows)
			{
				if (topWindow.windowLevel == UIWindowLevelNormal)
					break;
			}
		}
		
		UIView *rootView = [[topWindow subviews] objectAtIndex:0];
		id nextResponder = [rootView nextResponder];
		
		if ([nextResponder isKindOfClass:[UIViewController class]])
			result = nextResponder;
		else if ([topWindow respondsToSelector:@selector(rootViewController)] && topWindow.rootViewController != nil)
			result = topWindow.rootViewController;
		else
			ZAssert(false, "Could not find a root view controller.");
		
		return result;
	}
		//	static
}


namespace rad { namespace NativeVideoPlayer {
	
	const char * kDidFinishNotification = "kNativeVideoPlayerDidFinishNotification";
	
	void playVideo(const std::string& path, std::function<void (const std::string&)> completion)
	{
		@autoreleasepool {
			NSString* urlString = [NSString stringWithCString:path.c_str() encoding:NSUTF8StringEncoding];
			NSURL* url = [NSURL fileURLWithPath:urlString];
			MovieViewController* vc = [[MovieViewController alloc] initWithContentURL:url];
			MPMoviePlayerController* player = [vc moviePlayer];
			player.contentURL = url;
			player.movieSourceType = MPMovieSourceTypeFile;
			[player prepareToPlay];
			[player.view setFrame:[[UIScreen mainScreen] bounds]];
			
			const std::string path_copy(path);
			[vc setOnDismiss:^ {
				if (completion)
					completion(path_copy);
				cocos2d::CCNotificationCenter::sharedNotificationCenter()->postNotification(kNativeVideoPlayerDidFinishNotification, cocos2d::CCString::create(path_copy));
			}];
			
			[getRootViewController() presentMoviePlayerViewControllerAnimated:vc];
			[vc release];
			
//			[player play];
		}
	}
	
} }


