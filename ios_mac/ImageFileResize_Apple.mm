//
//  ImageFileResize_Apple.m
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#import "../ImageFileResizer.h"
/*
namespace
{
	inline
	void Convert(const rad::plat::ImageFileResizer::Size& cs, CGSize& cgs) {
		cgs.width = cs.width;
		cgs.height = cs.height;
	}
}

namespace rad { namespace plat {
	
	bool ImageFileResizer::readResizeWrite(const std::string& srcFile, const std::string& dstFile, const Size& size, const bool size_set)
	{
		bool ret = false;
		
		@autoreleasepool {
			
			NSString *src, *dst;
			UIImage *image;
			
			src = [NSString stringWithCString:srcFile.c_str() encoding:NSUTF8StringEncoding];
			dst = [NSString stringWithCString:dstFile.c_str() encoding:NSUTF8StringEncoding];
			
			image = [UIImage imageWithContentsOfFile:src];
			if (image)
			{
				CGSize newSize;
				UIImage *newImage;
				if (size_set)
				{
					const Size anewSize = sizeToFitInTexture(size.width, size.height);
					Convert(anewSize, newSize);
				}
				else
				{
						// use largest size that this device will support... yikes
					CGImageRef iref = image.CGImage;
					const Size anewSize = sizeToFitInTexture(CGImageGetWidth(iref), CGImageGetHeight(iref));
					Convert(anewSize, newSize);
				}
				
				UIGraphicsBeginImageContext(newSize);
				[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
				newImage = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
				
					// Error occurs if nothing to delete... So we just pass nullptr, since we don't care
				[[NSFileManager defaultManager] removeItemAtPath:dst error:nullptr];
					// Create directly with default file attributes.
					// Error occurs if directory already exists... So we just pass nullptr, since we don't care
				[[NSFileManager defaultManager] createDirectoryAtPath:[dst stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nullptr error:nullptr];
				
					// always produces an image
					//					if (!newImage)
					//					{
					//						DLog("No new Image");
					//					}
				
				
				
				if ([[[dst pathExtension] lowercaseString] isEqualToString:@"png"])
						// create png image
					ret = (newImage && [UIImagePNGRepresentation(newImage) writeToFile:dst atomically:YES]);
				else
						// create jpg image
					ret = (newImage && [UIImageJPEGRepresentation(newImage, 0.8) writeToFile:dst atomically:YES]);
				
				
			} // if image
		} // autoreleasepool
		
		return ret;
	}
	
} }

*/