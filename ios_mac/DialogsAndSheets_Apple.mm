//
//  DialogsAndSheets.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/11/14.
//
//

#import "../DialogsAndSheets.h"
#import <Foundation/Foundation.h>

extern template class std::vector<std::string>;

@interface JSC_Delegate : NSObject <UIAlertViewDelegate, UIActionSheetDelegate>
{
	@package
	rad::IndexedDialogDelegateRef del_obj;
}
+ (JSC_Delegate*)delegateWithIndexedDelegate:(rad::IndexedDialogDelegateRef)obj;
@end


static NSMutableSet* JSC_DELEGATES(){
	static NSMutableSet* set = nullptr;
	if (set == nullptr)
		set = [[NSMutableSet alloc] init];
	return set;
}



static JSC_Delegate* addDelegate(JSC_Delegate* d)
{
	[JSC_DELEGATES() addObject:d];
	return d;
}

static void removeDelegate(JSC_Delegate* d)
{
	[JSC_DELEGATES() removeObject:d];
}


namespace rad
{
	void CCConfirmationBox(const char * pszTitle, const char *  pszMsg, IndexedDialogDelegateRef delegate, const char * pszCancelBtnTitle, const char * pszConfirmBtnTitle, ConfirmationBoxStyle boxType, const char * textFieldDefault, const char * textFieldDefault2)
	{
		@autoreleasepool {
			JSC_Delegate* del = [JSC_Delegate delegateWithIndexedDelegate:delegate];
			UIAlertView* alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithCString:pszTitle encoding:NSUTF8StringEncoding]
															message:[NSString stringWithCString:pszMsg encoding:NSUTF8StringEncoding]
														   delegate:del
												  cancelButtonTitle:[NSString stringWithCString:pszCancelBtnTitle encoding:NSUTF8StringEncoding]
												  otherButtonTitles:[NSString stringWithCString:pszConfirmBtnTitle encoding:NSUTF8StringEncoding], nil];
//			if (boxType > UIAlertViewStyleLoginAndPasswordInput)
//				boxType = 0;
			[alert setAlertViewStyle:(UIAlertViewStyle)boxType];
			
			/**
			 *  textFieldAtIndex may throw an NSOutOfRange exception depending on the alert style.
			 */
			try {
				if (textFieldDefault != nullptr)
				{
					[[alert textFieldAtIndex:0] setText:[NSString stringWithCString:textFieldDefault encoding:NSUTF8StringEncoding]];
				}
				if (textFieldDefault2 != nullptr)
				{
					[[alert textFieldAtIndex:1] setText:[NSString stringWithCString:textFieldDefault2 encoding:NSUTF8StringEncoding]];
				}
			} catch (...) {
					// Fuck it
			}
			[alert show];
		}
	}
	

	void CCActionSheet(const char * title, IndexedDialogDelegateRef delegate, const char * cancelButton, const char * destructiveButton, const std::vector<std::string>& otherButtons)
	{
		@autoreleasepool {
			NSString *nstitle = nullptr, *cancel = nullptr, *destr = nullptr;
			
			JSC_Delegate* del = [JSC_Delegate delegateWithIndexedDelegate:delegate];
			
			nstitle = [NSString stringWithUTF8String:title];
			cancel = [NSString stringWithUTF8String:cancelButton];
			destr = [NSString stringWithUTF8String:destructiveButton];
			
			UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nstitle
															   delegate:del
													  cancelButtonTitle:cancel
												 destructiveButtonTitle:destr
													  otherButtonTitles:nullptr];
			NSString *nsi;
			for (const auto& b : otherButtons)
			{
				nsi = [NSString stringWithUTF8String:b.c_str()];
				[sheet addButtonWithTitle:nsi];
			}
			
			[sheet showInView:[[[UIApplication sharedApplication] delegate] window].rootViewController.view ];
		}
	}
}


@implementation JSC_Delegate

+ (JSC_Delegate*)delegateWithIndexedDelegate:(rad::IndexedDialogDelegateRef)obj
{
	JSC_Delegate* del = [[JSC_Delegate alloc] initWithIndexedDelegate:obj];
	return addDelegate(del);
}

- (id)initWithIndexedDelegate:(rad::IndexedDialogDelegateRef)obj
{
	self = [super init];
	if (self) {
		self->del_obj = obj;
	}
	return self;
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	@autoreleasepool {
		if (del_obj)
		{
			
			const char * t1 = nullptr, * t2 = nullptr;
			
			switch (alertView.alertViewStyle)
			{
				default:
				case UIAlertViewStyleDefault:
				{
					
				}
					break;
				case UIAlertViewStyleSecureTextInput:
				case UIAlertViewStylePlainTextInput:
				{
					t1 = [[[alertView textFieldAtIndex:0] text] UTF8String];
					
				}
					break;
				case UIAlertViewStyleLoginAndPasswordInput:
				{
					t1 = [[[alertView textFieldAtIndex:0] text] UTF8String];
					t2 = [[[alertView textFieldAtIndex:1] text] UTF8String];
				}
					break;
			}
			del_obj->clickedButtonAtIndex(static_cast<int>(buttonIndex), t1, t2);
			del_obj = nullptr;
		}
		removeDelegate(self);
		alertView.delegate = nullptr;
	}
}

- (void)alertViewCancel:(UIAlertView *)alertView
{
	[self alertView:alertView clickedButtonAtIndex:0];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex;
{
	@autoreleasepool {
		if (del_obj)
		{
			del_obj->clickedButtonAtIndex(static_cast<int>(buttonIndex));
			del_obj = nullptr;
		}
		removeDelegate(self);
		actionSheet.delegate = nullptr;
	}
	
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
	[self actionSheet:actionSheet clickedButtonAtIndex:0];
}
@end