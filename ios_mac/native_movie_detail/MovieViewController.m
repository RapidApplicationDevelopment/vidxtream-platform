//
//  MovieViewController.m
//  POV2
//
//  Created by Clayton Thomas on 10/9/13.
//
//

#import "MovieViewController.h"

@implementation MovieViewController

- (void)dealloc
{
	DLog("dealloc movie player");
    [super dealloc];
}
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	if (_onDismiss) {
		_onDismiss();
	}
}


#ifdef __IPHONE_6_0
	// For ios6, use supportedInterfaceOrientations & shouldAutorotate instead
- (NSUInteger) supportedInterfaceOrientations{

    return UIInterfaceOrientationMaskAllButUpsideDown;

}

- (BOOL) shouldAutorotate
{
	return YES;
}

#else
	// Override to allow orientations other than the default portrait orientation.
	// This method is deprecated on ios6
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
		//    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
}
#endif
@end
