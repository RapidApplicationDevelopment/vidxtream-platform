//
//  MovieViewController.h
//  POV2
//
//  Created by Clayton Thomas on 10/9/13.
//
//

#import <MediaPlayer/MediaPlayer.h>

typedef void(^MoviewViewControllerWillDismissBlock)(void);

@interface MovieViewController : MPMoviePlayerViewController
@property (nonatomic, copy) MoviewViewControllerWillDismissBlock onDismiss;
@end
