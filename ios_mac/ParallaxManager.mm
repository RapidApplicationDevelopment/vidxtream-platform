//
//  ParallaxManager.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#import "ParallaxManager.h"
#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
#import "cocos2d.h"

namespace
{
	static NSOperationQueue* opQueue = nullptr;
	
	CMMotionManager *sharedMotionManager()
	{
		static CMMotionManager *_sharedMotionManager = nullptr;
		if (_sharedMotionManager == nullptr)
		{
			_sharedMotionManager = [[CMMotionManager alloc] init];
			opQueue = [[NSOperationQueue alloc]	init];
			_sharedMotionManager.deviceMotionUpdateInterval = 1.0/30.0;
			opQueue.maxConcurrentOperationCount = 1;
		}
		return _sharedMotionManager;
	}
}

namespace rad
{
	bool ParallaxManager::deviceMotionAvailable()
	{
		CMMotionManager *motionManager = sharedMotionManager();
		return motionManager.deviceMotionAvailable;
	}
	
	bool ParallaxManager::startUpdating()
	{
		__block long referenceNotSet    = 1l;
        __block CGFloat referencePitch  = 0.0f;
        __block CGFloat referenceRoll   = 0.0f;
		CMMotionManager *motionManager = sharedMotionManager();
		const bool updAvail = motionManager.deviceMotionAvailable;
		if (!updAvail)
			return false;
		
		[motionManager setDeviceMotionUpdateInterval:1.0/30.0];
		[motionManager startDeviceMotionUpdatesToQueue:opQueue withHandler:^(CMDeviceMotion *motion, NSError *error) {
				// Set reference angles
			CMAttitude* attitude = [motion attitude];
            if (__builtin_expect(referenceNotSet, 0l))
            {
                referencePitch  = [attitude pitch];
                referenceRoll   = [attitude roll];
                
					//                referenceSet    = true;
				referenceNotSet = 0l;
				DLog("SET REFERENCE");
            }
            
				// Reference offset
            CGFloat _pitch      = [attitude pitch] - referencePitch;
            CGFloat _roll       = [attitude roll] - referenceRoll;
            
				// Update
				// @note Add orientation support here
			referencePitch = 0.95f*referencePitch + _pitch*0.05f;
			referenceRoll = 0.95f*referenceRoll + _roll*0.05f;
			
			if (_pitch > 0.49f)
			{
				_pitch = 0.49f;
			}
			else if (_pitch < -0.49f)
			{
				_pitch = -0.49f;
			}
			if (_roll > 0.49f)
			{
				_roll = 0.49f;
			}
			else if (_roll < -0.49f)
			{
				_roll = -0.49f;
			}
			
			ParallaxManager::updateMotionFromBackgroundThread(-_roll, _pitch);
        }];
		
		return true;
	}
	bool ParallaxManager::stopUpdating()
	{
		CMMotionManager *motionManager = sharedMotionManager();
		const bool updAvail = motionManager.deviceMotionAvailable;
		if (updAvail)
			[motionManager stopDeviceMotionUpdates];
		return updAvail;
	}
	bool ParallaxManager::setUpdateInterval(double i)
	{
		CMMotionManager *motionManager = sharedMotionManager();

		const bool updAvail = motionManager.deviceMotionAvailable;
		if (updAvail)
		{
			motionManager.deviceMotionUpdateInterval = i;
		}
		return updAvail;
	}
	
}
