//
//  Application_extensions_Apple.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#import "../Application_extensions.h"
#import "cocos2d.h"
#import <Foundation/Foundation.h>
#import "GSDropboxActivity.h"

namespace
{
	static
	UIViewController* getCurrentRootViewController() {
		
		UIViewController *result = nil;
		
			// Try to find the root view controller programmically
		
			// Find the top window (that is not an alert view or other window)
		UIWindow *topWindow = [[UIApplication sharedApplication] keyWindow];
		if (topWindow.windowLevel != UIWindowLevelNormal)
		{
			NSArray *windows = [[UIApplication sharedApplication] windows];
			for(topWindow in windows)
			{
				if (topWindow.windowLevel == UIWindowLevelNormal)
					break;
			}
		}
		
		UIView *rootView = [[topWindow subviews] objectAtIndex:0];
		id nextResponder = [rootView nextResponder];
		
		if ([nextResponder isKindOfClass:[UIViewController class]])
			result = nextResponder;
		else if ([topWindow respondsToSelector:@selector(rootViewController)] && topWindow.rootViewController != nil)
			result = topWindow.rootViewController;
		else
			ZAssert(NO, "Could not find a root view controller.");
		
		return result;
	}
}

namespace rad { namespace Application_extensions {
	
	bool openURL(const std::string& urlString)
	{
		@autoreleasepool {
			NSString* urls = [NSString stringWithCString:urlString.c_str() encoding:NSUTF8StringEncoding];
			NSURL *url = [NSURL URLWithString:urls];
			return [[UIApplication sharedApplication] openURL:url];
		}
	}
	bool canOpenURL(const std::string& urlString)
	{
		@autoreleasepool {
			NSString* urls = [NSString stringWithCString:urlString.c_str() encoding:NSUTF8StringEncoding];
			NSURL *url = [NSURL URLWithString:urls];
			return [[UIApplication sharedApplication] canOpenURL:url];
		}
	}
	void shareURL(const std::string& urlString)
	{
		@autoreleasepool {
			UIViewController* viewController = getCurrentRootViewController();
			if (ctlikely(viewController))
			{
				NSURL *url;
				NSString* urls = [NSString stringWithCString:urlString.c_str() encoding:NSUTF8StringEncoding];
					//							if (urlString.at(0) != '/')
					//								url = [NSURL URLWithString:urls];
					//							else
				if ((cocos2d::CCFileUtils::sharedFileUtils()->isFileExist(urlString)) == false)
					url = [NSURL URLWithString:urls];
				else
					url = [NSURL fileURLWithPath:urls];
				DLog("url to share = %@",url);
				NSArray *objectsToShare = @[
											url
											];
				NSArray *activities = @[
										[[[GSDropboxActivity alloc] init] autorelease]
										];
				
				UIActivityViewController *vc = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare
																				 applicationActivities:activities];
				
					// Exclude some default activity types to keep this demo clean and simple.
					//			vc.excludedActivityTypes = @[];
				if (ctlikely(vc))
				{
					[viewController presentViewController:vc animated:YES completion:NULL];
					[vc release];
				}
			}
		}
	}
	std::string getCacheDirectory()
	{
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
		if ([paths count] > 0)
		{
			return [(NSString*)[paths objectAtIndex:0] UTF8String];
		}
		return nullptr;
	}
	
} }
