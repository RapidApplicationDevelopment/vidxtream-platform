//
//  OpenGLContext.cpp
//  MacH264InDisplay
//
//  Created by Clayton Thomas on 5/2/14.
//
//

#include "../OpenGLContext.h"
#include "CCGL.h"
#include "EAGLView.h"

#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE

typedef EAGLContext GLContextType;

#else

typedef NSOpenGLContext GLContextType;

#endif

using namespace rad;

namespace
{
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
	GLContextType* CreateContext(GLContextType * shared_context = nullptr) {
		
		EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
		EAGLSharegroup* sg = nullptr;
		
		if (shared_context) {DLog("shared_context = %@",shared_context);
			api = shared_context.API;
			sg = shared_context.sharegroup;
		}
		return [[GLContextType alloc] initWithAPI:api sharegroup:sg];
	}
	GLContextType *GetCocos2dContext() {
		return [[EAGLView sharedEGLView] context];
	}
	
#else
	
	GLContextType* CreateContext(GLContextType * shared_context = nullptr) {
		@autoreleasepool {
			NSOpenGLPixelFormatAttribute pixelFormatAttributes[] = {
				NSOpenGLPFADoubleBuffer,
				NSOpenGLPFAAccelerated, 0,
				0
			};
			
			NSOpenGLPixelFormat *pixelFormat = [[NSOpenGLPixelFormat alloc] initWithAttributes:pixelFormatAttributes];
			if (pixelFormat == nil)
			{
				NSLog(@"Error: No appropriate pixel format found");
			}
				// TODO: Take into account the sharegroup
			NSOpenGLContext *context = [[NSOpenGLContext alloc] initWithFormat:pixelFormat shareContext:shared_context];
			
				//		NSAssert(context != nil, @"Unable to create an OpenGL context. The GPUImage framework requires OpenGL support to work.");
			return context;
		}
	}
	GLContextType *GetCocos2dContext() {
		return [[EAGLView sharedEGLView] openGLContext];
	}
#endif
}

class OpenGLContext::Impl
{
	GLContextType *context;
	
public:

	~Impl()
	{
		freeContext();
	}
	Impl()
	: context(nullptr)
	{
		
	}
	
	bool init(OpenGLContext* shared) {
		context = CreateContext(shared->impl->context);
		return context != nullptr;
	}
	
	bool init(GLContextType* shared) {
		context = CreateContext(shared);
		return context != nullptr;
	}
	
	bool isCurrent() const {
		return ([GLContextType currentContext] == this->context);
	}
	
	bool makeCurrent() {
		auto ret = false;
		@autoreleasepool {
			
			if (false == isCurrent()) {
				DLog("");
				glFlush();
				glBindTexture(GL_TEXTURE_2D, 0);
				setCurrentContext(this->context);
				ret = true;
			}
		}
		return ret;
	}
	
private:
	void freeContext() {
		if (context) {
			if (true == isCurrent())
				setCurrentContext(nullptr);
#if !CTBuiltWithARC
			[context release];
#endif
		}
		context = nullptr;
	}
	
public:
	static
	void setCurrentContext(GLContextType *c) {
#if TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE
		[GLContextType setCurrentContext:c];
#else
#warning "WHAT DO DO HERE WHEN c === NULL?"
		if (c)
			[c makeCurrentContext];
#endif
	}
};


OpenGLContext::~OpenGLContext()
{

}

OpenGLContext::OpenGLContext()
: impl(new OpenGLContext::Impl())
{
	
}

bool OpenGLContext::init()
{
	return initWithSharedContext(nullptr);
}
bool OpenGLContext::initWithSharedContext(OpenGLContext *shared)
{
	return impl->init(shared);
}

bool OpenGLContext::initWithCocos2dAsSharedContext()
{
	return impl->init(GetCocos2dContext());
}
bool OpenGLContext::isCurrent() const
{
	return impl->isCurrent();
}
bool OpenGLContext::makeCurrent()
{
	return impl->makeCurrent();
}

bool OpenGLContext::setCurrentContext(OpenGLContext* context)
{
	if (context) {
		return context->makeCurrent();
	}
	OpenGLContext::Impl::setCurrentContext(nullptr);
	return true;
}