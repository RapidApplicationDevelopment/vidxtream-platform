
package net.rapidappdevel;

import java.io.File;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class AppExtensionHelper {

	public static final int PLAY_VIDEO_REQUEST_TAG = 0;
	public static final int SHARE_MEDIA_REQUEST_TAG = 1;
	private static final String TAG = AppExtensionHelper.class.getSimpleName();
	
	private static Context mContext;


	public static void setContext(Context c) {
		this.mContext = c;
	}
	
	private static Context getContext() {
		return this.mContext;
	}


	private static String getTypeFromString(final String url) {
			// if video... video/*
			// if photo... image/*
		if (url.endsWith("png"))
		{
			return "image/png";
		}
		if (url.endsWith("jpg") || url.endsWith("jpeg"))
		{
			return "image/jpeg";
		}
		
		if (url.endsWith("mp4") || url.endsWith("MP4"))
		{
			return "video/mp4";
		}
		
		// Don't use text/plain
		return "*/*";
	}

	private static Uri getUriFromPath(final String path) {
		if (path.startsWith("http://"))
			return Uri.parse(path);
		else
			return Uri.fromFile(new File(path));
	}

	private static Intent createShareIntent(final String path) {
	    Intent shareIntent = new Intent(Intent.ACTION_SEND);
	    shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
	    final String tp = getTypeFromString(path);
	    shareIntent.setType(tp);

	    // For a file in shared storage.  For data in private storage, use a ContentProvider.
	    final Uri uri = getUriFromPath(path);

	    shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
	    return shareIntent;
	}  


	// ===========================================================
	// Called from native code
	// - shareURL
	// - canOpenURL
	// - openURL
	// - playVideo
	// ===========================================================
	public static void shareURL(final String url) {
		Intent intent = createShareIntent(url);
		
		((Activity) getContext()).startActivityForResult(Intent.createChooser(intent, "Share via"), SHARE_MEDIA_REQUEST_TAG);
	}
	
	public static boolean canOpenURL(final String url) {
		return false;
	}
	
	public static boolean openURL(final String url) {
		return false;
	}

	private static Intent createPlayerIntent(final String path) {
	    Intent playerIntent = new Intent(Intent.ACTION_VIEW);

	    Uri uri = getUriFromPath(path);

	    playerIntent.setType("video/mp4");

	    // For a file in shared storage.  For data in private storage, use a ContentProvider.
	    playerIntent.putExtra(Intent.EXTRA_STREAM, uri);
	    return playerIntent;
	}  

	public static void playVideo(final String url) {
		Intent intent = createPlayerIntent(url);
		
		((Activity) getContext()).startActivityForResult(intent, PLAY_VIDEO_REQUEST_TAG);
	}

		// ===========================================================
		// Methods
		// Native method
		// ===========================================================
	
	public static native void onVideoFinished();
	
}
