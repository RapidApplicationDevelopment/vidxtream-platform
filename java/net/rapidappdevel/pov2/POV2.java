/****************************************************************************
Copyright (c) 2010-2012 cocos2d-x.org

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package net.rapidappdevel.pov2;

import org.cocos2dx.lib.Cocos2dxActivity;

import android.app.Activity;
import android.content.Intent;
/* we got rid of this in Piper */
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import net.rapidappdevel.pov2.R;
import net.rapidappdevel.DialogHelper;
import net.rapidappdevel.OrientationHelper;
import net.rapidappdevel.AppExtensionHelper;

import org.cocos2dx.plugin.PluginWrapper;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

public class POV2 extends Cocos2dxActivity implements OrientationHelper.OrientationHelperListener, DialogHelper.DialogHelperListener {

	private static final String TAG = POV2.class.getSimpleName();
	private OrientationHelper mOrientationHelper;

	/* we got rid of this in Piper */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DialogHelper.setListener(this);
		this.mOrientationHelper = new OrientationHelper(this,this);
		this.mOrientationHelper.enable();
		AppExtensionHelper.setContext(this);
	}

	public Cocos2dxGLSurfaceView onCreateView() {
    	Cocos2dxGLSurfaceView glSurfaceView = new Cocos2dxGLSurfaceView(this);
			// Piper should create stencil buffer
    	glSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 8);
    	PluginWrapper.init(this);
		PluginWrapper.setGLSurfaceView(glSurfaceView);
    	return glSurfaceView;
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void orientationChanged(final int fromOrientation, final int toOrientation) {
		if (toOrientation != OrientationHelper.UPSIDE_DOWN) {
			// Called from the UI thread
			// Need to switch to the native GL thread
			this.runOnGLThread(new Runnable() {
				@Override
			    public void run() {
					OrientationHelper.onOrientationChanged(fromOrientation, toOrientation);	
				}
		    });
		}
	}
	
	@Override
	public void dialogDismissed(final int index, final String s1, final String s2) {
		// Called from the UI thread
		// Need to switch to the native GL thread
		this.runOnGLThread(new Runnable() {
			@Override
		    public void run() {
				DialogHelper.onDialogDismissed(index, s1, s2);	
			}
	    });
		
	}
	
	@Override
	public Activity getActivity() {
		return this;
	}
	
	
	@Override
	public void onPause() {
	    super.onPause();  // Always call the superclass method first
	    this.mOrientationHelper.disable();
	}
	@Override
	public void onResume() {
	    super.onResume();  // Always call the superclass method first
	    this.mOrientationHelper.enable();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode,Intent data) {
		Log.d(TAG, "Video finished playing");
        if (requestCode == AppExtensionHelper.PLAY_VIDEO_REQUEST_TAG) {
			
			AppExtensionHelper.onVideoFinished();
			
        }
    }
	
	static {
        System.loadLibrary("cocos2djs");
    }
}
