
package net.rapidappdevel;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class OrientationHelper implements SensorEventListener {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final String TAG = OrientationHelper.class.getSimpleName();

		// ===========================================================
		// Fields
		// ===========================================================
	private int mOrientationDeg; //last rotation in degrees
	private int mOrientationRounded; //last orientation int from above
	private int ORIENTATION_UNKNOWN = -1;

	private final OrientationHelperListener mListener;
	private final SensorManager mSensorManager;
	private final Sensor mAccelerometer;
	
	public static final int PORTRAIT = 1;
	public static final int UPSIDE_DOWN = 2;
	public static final int LANDSCAPE_RIGHT = 3; // home on right
	public static final int LANDSCAPE_LEFT = 4; // home on left

	private static final int _DATA_X = 0;
	private static final int _DATA_Y = 1;
	private static final int _DATA_Z = 2;
	
		// ===========================================================
		// Constructors
		// ===========================================================

	public OrientationHelper(final Context pContext, final OrientationHelperListener pListener) {
		
		this.mListener = pListener;
		this.mOrientationRounded = ORIENTATION_UNKNOWN;
		this.mSensorManager = (SensorManager) pContext.getSystemService(Context.SENSOR_SERVICE);
		this.mAccelerometer = this.mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

	}

		// ===========================================================
		// Getter & Setter
		// ===========================================================

	public void enable() {
		this.mSensorManager.registerListener(this, this.mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	public void disable() {
		this.mSensorManager.unregisterListener(this);
	}
	
	public int getOrientation() {
		return mOrientationRounded;
	}

		// ===========================================================
		// Methods for/from SuperClass/Interfaces
		// ===========================================================

	@Override
	public void onSensorChanged(final SensorEvent pSensorEvent) {
		if (pSensorEvent.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
			return;
		}
		
		float[] values = pSensorEvent.values;
	    
		int orientation = this.calculateOrientationFromSensorValues(values);
			
	    int tempOrientRounded = this.getRoundedOrientation(orientation);
		
		this.setOrientation(tempOrientRounded, true);
	}
	
	@Override
	public void onAccuracyChanged(final Sensor pSensor, final int pAccuracy) {
	}
	
	private int calculateOrientationFromSensorValues(final float[] values) {
		int orientation = ORIENTATION_UNKNOWN;
	    float X = -values[_DATA_X];
	    float Y = -values[_DATA_Y];
	    float Z = -values[_DATA_Z];
	    float magnitude = X*X + Y*Y;
			// Don't trust the angle if the magnitude is small compared to the y value
	    if (magnitude * 4 >= Z*Z) {
	        float OneEightyOverPi = 57.29577957855f;
	        float angle = (float)Math.atan2(-Y, X) * OneEightyOverPi;
	        orientation = 90 - (int)Math.round(angle);
				// normalize to 0 - 359 range
	        while (orientation >= 360) {
	            orientation -= 360;
	        }
	        while (orientation < 0) {
	            orientation += 360;
	        }
	    }
		//^^ thanks to google for that code
		return orientation;
	}
	
	private int getRoundedOrientation(final int orientation) {
		// now we must figure out which orientation based on the degrees
		
		int tempOrientRounded = 0;
		if (orientation != mOrientationDeg)
	    {
	        mOrientationDeg = orientation;
				//figure out actual orientation
	        if(orientation == -1){//basically flat
				
	        }
	        else if(orientation <= 45 || orientation > 315){//round to 0
	            tempOrientRounded = PORTRAIT;//portrait
	        }
	        else if(orientation > 45 && orientation <= 135){//round to 90
	            tempOrientRounded = LANDSCAPE_LEFT; //lsleft
	        }
	        else if(orientation > 135 && orientation <= 225){//round to 180
	            tempOrientRounded = UPSIDE_DOWN; //upside down
	        }
	        else if(orientation > 225 && orientation <= 315){//round to 270
	            tempOrientRounded = LANDSCAPE_RIGHT;//lsright
	        }
			
	    }
		return tempOrientRounded;
	}
	
	private void setOrientation(final int tempOrientRounded, final boolean notify) {
		if(tempOrientRounded != 0 && mOrientationRounded != tempOrientRounded){
	            //Orientation changed, handle the change here
			final int tmpold = mOrientationRounded;
	        mOrientationRounded = tempOrientRounded;
			if (notify && this.mListener != null) {
				this.mListener.orientationChanged(tmpold, tempOrientRounded);
			}
	    }
	}

	

		// ===========================================================
		// Methods
		// Native method called from Cocos2dxGLSurfaceView (To be in the same thread)
		// ===========================================================

	public static native void onOrientationChanged(final int fromOrientation, final int toOrientation);

		// ===========================================================
		// Inner and Anonymous Classes
		// ===========================================================
	public static interface OrientationHelperListener {
		public void orientationChanged(final int fromOrientation, final int toOrientation);
	}
}
