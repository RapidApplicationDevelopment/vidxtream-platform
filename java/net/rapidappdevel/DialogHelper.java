package net.rapidappdevel;

import android.content.Context;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

import java.util.ArrayList;

import net.rapidappdevel.pov2.R;


public class DialogHelper {
	
	private static DialogHelperListener listener = null;
	
	private static Activity getUIContext() {
		return getListener().getActivity();
	}
	public static void setListener(DialogHelperListener ll) {
		listener = ll;
	}
	private static DialogHelperListener getListener() {
		return listener;
	}

	//----------------------------------------------------------------------------------------
	// Called from native code
	//----------------------------------------------------------------------------------------
	public static void showActionSheet(final String title, final String cancelButton, final String confirmButton, final ArrayList<String> stringArray ) {
		// Called from background thread
		// Need to move to UI thread before continuing
		getUIContext().runOnUiThread(new Runnable() {
			@Override
		    public void run() {

					_showPopupMenu(title, cancelButton, confirmButton, stringArray);
					
			}
	    });
	}
	

	private static PopupMenu _createPopupMenu() {
		final PopupWindow popupWindow = new PopupWindow(getUIContext());
		
		View view; 

		LayoutInflater inflater = (LayoutInflater)   getUIContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		
		view = inflater.inflate(R.layout.custom2, null);
		
		popupWindow.setFocusable(true);
		popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
		popupWindow.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
		popupWindow.setContentView(view);
		
		return new PopupMenu(getUIContext(), view.findViewById(R.id.alert_anchor));
	}

	private static void _showPopupMenu(final String title, final String cancelButton, final String confirmButton, final ArrayList<String> stringArray ){
		
		final PopupMenu popupMenu = _createPopupMenu();
		
		Menu menu = popupMenu.getMenu();
		
		int i,j, end = stringArray.size();
		
		menu.add(0, 0, 0, title);
		// these are swapped on Android (relative to iOS)
		menu.add(1,1,1, confirmButton);
		menu.add(2,2,2, cancelButton);
		
		for(i=0; i < end; ++i) {
			j = i + 3;
			menu.add(j, j, j, stringArray.get(i));	//Add all of the items to the menu with the index associated to the menu item
		}
		
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				
				popupMenu.dismiss();			//exit the popUpMenu
				int which = item.getItemId() - 1;
				_onDialogDismissed(which,null,null);

				return true;
			}
		});
    
		popupMenu.show();
	}
	
	//----------------------------------------------------------------------------------------
	// Called from native code
	//----------------------------------------------------------------------------------------
	public static void CreatingDialogBox(final String title, final String message, final String CancelBtnTitle, final String ConfirmBtnTitle, final int type, final String textPlaceholder1, final String textPlaceholder2) {
		// Called from background thread
		// Need to move to UI thread before continuing
		getUIContext().runOnUiThread(new Runnable() {
			@Override
		    public void run() {
				
				switch(type) {
					default:
					// fallthrough to case 0 (Default)
					case 0: 
					{
						//DEFAULT
						CreatingDialogBoxForDefault(title, ConfirmBtnTitle, CancelBtnTitle , message);
					} break;
					case 1: 
					{
						//SECURE
						CreatingDialogBoxForSecure(title, ConfirmBtnTitle, CancelBtnTitle, message, textPlaceholder1);
					} break;
					case 2: 
					{
						//PLAIN
						CreatingDialogBoxForPlain(title, ConfirmBtnTitle, CancelBtnTitle, message, textPlaceholder1);
					} break;
					case 3: 
					{
						//LOGIN_AND_PASS
						CreatingDialogBoxForLoginAndPass(title, ConfirmBtnTitle, CancelBtnTitle, message, textPlaceholder1, textPlaceholder2);
					} break;
				
				} // end of switch(type)	
				
			}
	    });
	}
	
	
	
	//----------------------------------------------------------------------------------------
	//create only the basic of the dialogBox: Message, title, confirm button text
	//-------------------------------------------------------------------------------------------
	public static void CreatingDialogBoxForDefault(String title, String ConfirmBtnTitle, String CancelBtnTitle, String message ){
		
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getUIContext());		//declare/dynamically the dialog box
		alertDialogBuilder.setTitle(title);	//set title
		
		alertDialogBuilder
		.setMessage(message).setCancelable(false).			//it cannot be cancelled
		setPositiveButton( CancelBtnTitle , new DialogInterface.OnClickListener() {				//this is not an error: positive button is
				//set to "Cancel" to match up more to the original version
			@Override																	//to have the cancel in the left
			public void onClick(DialogInterface dialog, int which) {
				
				_onDialogDismissed(0, null, null);			//for Cancel
				dialog.cancel();
					//MainActivity.this.finish();		//exit the app
				
			}
		}).setNegativeButton(ConfirmBtnTitle, new DialogInterface.OnClickListener(){			//this is not an error: negative button is
				//set to the confirm button to match up more to the original version
				//to have the cancel in the right
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				_onDialogDismissed(1, null, null);		//for confirm
				dialog.cancel();		//exit the dialog box
			}
			
			
			
		});
		
			//has to be AFTER SETTINGS
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();		//create and show it
		
		
	}
	//--------------------------------------------------------------------------------------End creating dialog box for default

	//--------------------------------------------------------------------------------------
	//need to create layout and editable text
	public static void CreatingDialogBoxForPlain(String title, String ConfirmBtnTitle, String CancelBtnTitle, String message, String placeholder){
		
		final Dialog dialogobj_Secure = new Dialog(getUIContext());
		dialogobj_Secure.setContentView(R.layout.custom);		//load/set dialog
		dialogobj_Secure.setTitle(title);					//set title
		
		final EditText LoginDialog;
		
			// set the custom dialog components
		TextView text = (TextView) dialogobj_Secure.findViewById(R.id.text);
		text.setText(message);
		
		Button diaButton2 = (Button) dialogobj_Secure.findViewById(R.id.dialogButtonCancel);
		diaButton2.setText(CancelBtnTitle);
		
		Button diaButton = (Button) dialogobj_Secure.findViewById(R.id.dialogButtonOK);
		diaButton.setText(ConfirmBtnTitle);
		
			//using the same layout than the password to avoid re creation of layout.
		LoginDialog = (EditText) dialogobj_Secure.findViewById(R.id.txtPassword);
		LoginDialog.setInputType(1);							//change the type of the editable text to normal text instead of password
		LoginDialog.setText(placeholder);
		
		diaButton.setOnClickListener(new OnClickListener(){		//for confirm: grab the Login
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(1, LoginDialog.getText().toString(), null);
				dialogobj_Secure.dismiss();		//exit the dialog
				
					//Toast.makeText(context, LoginDialog.getText().toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		diaButton2.setOnClickListener(new OnClickListener(){		//for Cancel: just leave the dialog
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(0, null, null);
				dialogobj_Secure.dismiss();			//exit the dialog
			}
		});
		
		dialogobj_Secure.show();
		
	}


	//--------------------------------------------------------------------------------------End CreatingDialogBoxForPlain

	//--------------------------------------------------------------------------------------

	public static void CreatingDialogBoxForSecure(String title, String ConfirmBtnTitle, String CancelBtnTitle, String message, String placeholder){
		
		final Dialog dialogobj_Secure = new Dialog(getUIContext());
		dialogobj_Secure.setContentView(R.layout.custom);		//load/set dialog
		dialogobj_Secure.setTitle(title);					//set title
		
		final EditText passwordDialog;
		
			// set the custom dialog components
		TextView text = (TextView) dialogobj_Secure.findViewById(R.id.text);
		text.setText(message);
		
		Button diaButton = (Button) dialogobj_Secure.findViewById(R.id.dialogButtonOK);
		diaButton.setText(ConfirmBtnTitle);
		
		Button diaButton2 = (Button) dialogobj_Secure.findViewById(R.id.dialogButtonCancel);
		diaButton2.setText(CancelBtnTitle);
		
		passwordDialog = (EditText) dialogobj_Secure.findViewById(R.id.txtPassword);
		passwordDialog.setText(placeholder);
		
		diaButton.setOnClickListener(new OnClickListener(){		//for confirm: grab the password
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(1, passwordDialog.getText().toString(), null);
				dialogobj_Secure.dismiss();		//exit the dialog
				
					//Toast.makeText(context, passwordDialog.getText().toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		diaButton2.setOnClickListener(new OnClickListener(){		//for Cancel: just leave the dialog
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(0, null, null);
				dialogobj_Secure.dismiss();			//exit the dialog
			}
		});
		
		dialogobj_Secure.show();
		
	}


	//--------------------------------------------------------------------------------------End CreatingDialogBoxForSecure

	//--------------------------------------------------------------------------------------
	public static void CreatingDialogBoxForLoginAndPass(String title, String ConfirmBtnTitle, String CancelBtnTitle, String message, String placeholder1, String placeholder2){
		
		final Dialog dialogobj_LoginAndPass = new Dialog(getUIContext());
		dialogobj_LoginAndPass.setContentView(R.layout.customloginpassword);		//load/set dialog
		dialogobj_LoginAndPass.setTitle(title);									//set title
		
		final EditText passwordEditable;
		final EditText loginEditable;
		
			// set the custom dialog components
		TextView text = (TextView) dialogobj_LoginAndPass.findViewById(R.id.textLoginPasswod);
		text.setText(message);
		
		Button diaButton = (Button) dialogobj_LoginAndPass.findViewById(R.id.LoginPasswordOKBtn);
		diaButton.setText(ConfirmBtnTitle);
		
		Button diaButton2 = (Button) dialogobj_LoginAndPass.findViewById(R.id.LoginPasswordCancelBtn);
		diaButton2.setText(CancelBtnTitle);
		
		passwordEditable = (EditText) dialogobj_LoginAndPass.findViewById(R.id.EdiPassword);
		loginEditable = (EditText) dialogobj_LoginAndPass.findViewById(R.id.EditLogin);
		
		loginEditable.setText(placeholder1);
		passwordEditable.setText(placeholder2);
		
		diaButton.setOnClickListener(new OnClickListener(){		//for confirm: grab the password
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(1, loginEditable.getText().toString(), passwordEditable.getText().toString());
				dialogobj_LoginAndPass.dismiss();		//exit the dialog
				
					//Toast.makeText(context, passwordDialog.getText().toString(), Toast.LENGTH_SHORT).show();
			}
		});
		
		diaButton2.setOnClickListener(new OnClickListener(){		//for Cancel: just leave the dialog
			
			@Override
			public void onClick(View v) {
					// TODO Auto-generated method stub
				
				_onDialogDismissed(0, null, null);
				dialogobj_LoginAndPass.dismiss();			//exit the dialog
			}
		});
		
		dialogobj_LoginAndPass.show();
		
		
	}

	// forward to native GL thread
	private static void _onDialogDismissed(final int index, final String s1, final String s2) {
		DialogHelperListener l = getListener();
		if (l != null)
			l.dialogDismissed(index, s1, s2);
	}
	

	//--------------------------------------------------------------------------------------
	// Native method
	//--------------------------------------------------------------------------------------
	public static  native  void onDialogDismissed(final int index, final String s1, final String s2);
	
	//--------------------------------------------------------------------------------------
	// Interface for dialog event delegates
	//--------------------------------------------------------------------------------------
	public static interface DialogHelperListener {
		public void dialogDismissed(final int index, final String s1, final String s2);
		public Activity getActivity();
	}

}
