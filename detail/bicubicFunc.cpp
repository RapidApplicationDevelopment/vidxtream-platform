
	// I may drop a thread_group in here to multithread resize... or just use bilinear

#include "bicubicFunc.h"
#include <cassert>
#include <cmath>

	//#define DEBUG_BICUBIC_FUNC

#ifdef DEBUG_BICUBIC_FUNC
namespace cocos2d {
	extern void CCLog(const char * pszFormat, ...);
}
#define ErrLog(format, ...) cocos2d::CCLog(format, ##__VA_ARGS__)
	//#include <stdio.h>
	//#define ErrLog(...) do { fprintf(stderr,"%s ",__PRETTY_FUNCTION__); fprintf(stderr,__VA_ARGS__); fprintf(stderr,"\n"); fflush(stderr);} while(0)
#else
#define ErrLog(...) do { } while(0)
#endif

using namespace std;

extern template class std::vector<unsigned char>;

extern "C" {
	typedef struct {
		unsigned char r,g,b,a;
	} RGBA;
	
	typedef struct {
		unsigned char r,g,b;
	} RGB;
}

namespace
{
		// Should be use Single or Double precision floating point numbers? -ffast-math is on...
		// We started with double, but see the same results with single... so...
	typedef float FloatType;
	
	FloatType BCR(const FloatType x)
	{
		FloatType xp2,xp1,xm1;
		FloatType r = 0;
		
		xp2 = x + 2;
		xp1 = x + 1;
		xm1 = x - 1;
		
		if (xp2 > 0)
			r += xp2 * xp2 * xp2;
		if (xp1 > 0)
			r -= 4 * xp1 * xp1 * xp1;
		if (x > 0)
			r += 6 * x * x * x;
		if (xm1 > 0)
			r -= 4 * xm1 * xm1 * xm1;
#define BCR_VI ((FloatType)(1.0/6.0))
		return(r * BCR_VI);
	}
	
	void BiCubicScaleRGBA(
						  const RGBA * __restrict__ bm_in, const int nx, const int ny,
						  RGBA * __restrict__ bm_out, const int nnx, const int nny)
	{
		int i_out,j_out,i_in,j_in,ii,jj;
		int n,m;
		long index;
//		FloatType cx,cy,dx,dy,weight;
		FloatType weight;
		FloatType red,green,blue,alpha;
		const FloatType nxInnx = nx / (FloatType)nnx;
		const FloatType nyInny = ny / (FloatType)nny;
		RGBA col;
			// I flipped these... down y, over x... I originally though over x, down y would be better, but why?
		for (j_out=0;j_out<nny;++j_out) {
			for (i_out=0;i_out<nnx;++i_out) {
					// probably should do this check
				i_in = i_out * nxInnx;
				j_in = j_out * nyInny;
//				i_in = (i_out * nx) / nnx;
//				j_in = (j_out * ny) / nny;
//				cx = i_out * nxInnx;
//				cy = j_out * nyInny;
//				dx = cx - i_in;
//				dy = cy - j_in;
				red   = 0;
				green = 0;
				blue  = 0;
				alpha = 0;
				for (m=-1;m<=2;m++) {
					for (n=-1;n<=2;n++) {
						ii = i_in + m;
						jj = j_in + n;
						if (ii < 0)   ii = 0;
						if (ii >= nx) ii = nx-1;
						if (jj < 0)   jj = 0;
						if (jj >= ny) jj = ny-1;
						index = jj * nx + ii;
						weight = BCR(m) * BCR(n);
//						weight = BCR(m-dx) * BCR(n-dy);
						red   += weight * bm_in[index].r;
						green += weight * bm_in[index].g;
						blue  += weight * bm_in[index].b;
						alpha += weight * bm_in[index].a;
					}
				}
				col.r = (int)red;
				col.g = (int)green;
				col.b = (int)blue;
				col.a = (int)alpha;
				bm_out[j_out * nnx + i_out] = col;
			}
		}
	}
	
	void BiCubicScaleRGB(
						 const RGB * __restrict__ bm_in, const int nx, const int ny,
						 RGB * __restrict__ bm_out, const int nnx, const int nny)
	{
		int i_out,j_out,i_in,j_in,ii,jj;
		int n,m;
		long index;
//		FloatType cx,cy,dx,dy,weight;
		FloatType weight;
		FloatType red,green,blue;
		const FloatType nxInnx = nx / (FloatType)nnx;
		const FloatType nyInny = ny / (FloatType)nny;
		RGB col;
		
			// I flipped these... down y, over x... I originally though over x, down y would be better, but why?
		for (j_out=0;j_out<nny;++j_out) {
			for (i_out=0;i_out<nnx;++i_out) {
					// probably should do this check
				i_in = i_out * nxInnx;
				j_in = j_out * nyInny;
//				i_in = (i_out * nx) / nnx;
//				j_in = (j_out * ny) / nny;
//				cx = i_out * nxInnx;
//				cy = j_out * nyInny;
//				dx = cx - i_in;
//				dy = cy - j_in;
				red   = 0;
				green = 0;
				blue  = 0;
				
				for (m=-1;m<=2;++m) {
					for (n=-1;n<=2;++n) {
						ii = i_in + m;
						jj = j_in + n;
						if (ii < 0)   ii = 0;
						if (ii >= nx) ii = nx-1;
						if (jj < 0)   jj = 0;
						if (jj >= ny) jj = ny-1;
						index = jj * nx + ii;
						weight = BCR(m) * BCR(n);
//						weight = BCR(m-dx) * BCR(n-dy);
						red   += weight * bm_in[index].r;
						green += weight * bm_in[index].g;
						blue  += weight * bm_in[index].b;
					}
				}
				col.r = (int)red;
				col.g = (int)green;
				col.b = (int)blue;
				bm_out[j_out * nnx + i_out] = col;
			}
		}
	}
	
	
	
	
	
	FloatType lerp(FloatType s, FloatType e, FloatType t){return s+(e-s)*t;}
	FloatType blerp(FloatType c00, FloatType c10, FloatType c01, FloatType c11, FloatType tx, FloatType ty){
		return lerp(lerp(c00, c10, tx), lerp(c01, c11, tx), ty);
	}
	template <typename PixelType>
	const PixelType& getpixel(const PixelType *image, unsigned int w, unsigned int x, unsigned int y){
		return image[(y*w)+x];
	}
	template <typename PixelType>
	void putpixel(PixelType *image, unsigned int w, unsigned int x, unsigned int y, const PixelType& result){
		image[(y*w)+x] = result;
	}
	void BiLinearScaleRGBA(
						  const RGBA * __restrict__ bm_in, const int nx, const int ny,
						  RGBA * __restrict__ bm_out, const int nnx, const int nny)
	{
		int x, y;
		RGBA col;
		RGBA c00,c10,c01,c11;
		
		col.a = 255;
		
		for(x= 0, y=0; y < nny; x++){
			if(x >= nnx){
				x = 0; y++;
				if (y >= nny)
					break;
			}
			FloatType gx = x / (FloatType)(nnx) * (nx-1);
			FloatType gy = y / (FloatType)(nny) * (ny-1);
			int gxi = (int)gx;
			int gyi = (int)gy;
			
			c00 = getpixel(bm_in, nx, gxi, gyi);
			c10 = getpixel(bm_in, nx, gxi+1, gyi);
			c01 = getpixel(bm_in, nx, gxi, gyi+1);
			c11 = getpixel(bm_in, nx, gxi+1, gyi+1);
			
			col.r = blerp(c00.r, c10.r, c01.r, c11.r, gx - gxi, gy - gyi);
			col.g = blerp(c00.g, c10.g, c01.g, c11.g, gx - gxi, gy - gyi);
			col.b = blerp(c00.b, c10.b, c01.b, c11.b, gx - gxi, gy - gyi);

			putpixel(bm_out, nnx,x, y, col);
		}
	}
	void BiLinearScaleRGB(
						  const RGB * __restrict__ bm_in, const int nx, const int ny,
						  RGB * __restrict__ bm_out, const int nnx, const int nny)
	{
		int x, y;
		RGB col;
		RGB c00,c10,c01,c11;
				
		for(x= 0, y=0; y < nny; x++){
			if(x >= nnx){
				x = 0; y++;
				if (y >= nny)
					break;
			}
			FloatType gx = x / (FloatType)(nnx) * (nx-1);
			FloatType gy = y / (FloatType)(nny) * (ny-1);
			int gxi = (int)gx;
			int gyi = (int)gy;
			
			c00 = getpixel(bm_in, nx, gxi, gyi);
			c10 = getpixel(bm_in, nx, gxi+1, gyi);
			c01 = getpixel(bm_in, nx, gxi, gyi+1);
			c11 = getpixel(bm_in, nx, gxi+1, gyi+1);
			
			col.r = blerp(c00.r, c10.r, c01.r, c11.r, gx - gxi, gy - gyi);
			col.g = blerp(c00.g, c10.g, c01.g, c11.g, gx - gxi, gy - gyi);
			col.b = blerp(c00.b, c10.b, c01.b, c11.b, gx - gxi, gy - gyi);
			
			putpixel(bm_out, nnx,x, y, col);
		}
	}
}



namespace rad { namespace plat { namespace detail {
	
	std::vector<unsigned char> bicubicresize(const std::vector<unsigned char>& in,
											 std::size_t src_width, std::size_t src_height, std::size_t dest_width, std::size_t dest_height, int channels)
	{
		std::vector<unsigned char> result(dest_height*dest_width*channels);
		
		if (channels == 4) {
			
			BiLinearScaleRGBA((RGBA *)in.data(), (int)src_width, (int)src_height, (RGBA *)result.data(), (int)dest_width, (int)dest_height);
			
		} else {
				// channels == 3
			BiLinearScaleRGB((RGB *)in.data(), (int)src_width, (int)src_height, (RGB *)result.data(), (int)dest_width, (int)dest_height);
			
		}
		
		return move(result);
	}
	
	std::vector<unsigned char> rgb2rgba(const std::vector<unsigned char>& data)
	{
		typedef std::vector<unsigned char> ArrType;
		const auto siz = data.size();
		const auto sizeInPixels = siz/3;
		
		ArrType result;
		result.reserve(sizeInPixels*4);
		
		for(ArrType::size_type i = 0; i < siz; i += 3) {
			
			result.push_back(data[i]);
			result.push_back(data[i+1]);
			result.push_back(data[i+2]);
			result.push_back(((unsigned char)255));
			
		}
		
		return move(result);
	}
	
} } }

#ifdef TEST_BICUBIC_FUNC

#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include "pixels.h"

int main(){

    std::vector<unsigned char> vec(&heart_raw[0], &heart_raw[sizeof(heart_raw)]), outputVec;

    outputVec = rad::plat::detail::bicubicresize(vec, 80, 80, 80/4, 80/4);

    std::ofstream outputFile;
    bool ret = true;
    outputFile.open("newPixel.raw");
    if(!outputFile.is_open()){
        ret = false;
    }
    else {
        std::string str(outputVec.begin(),outputVec.end());
        outputFile << str;
        outputFile.close();
    }
    std::cout << ret << std::endl;


    return 0;
}
#endif
