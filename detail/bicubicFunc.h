//
//  bicubicFunc.h
//  
//
//  Created by Clayton Thomas on 6/27/14.
//
//

#ifndef _bicubicFunc_h
#define _bicubicFunc_h

#include <vector>

namespace rad { namespace plat { namespace detail { 

std::vector<unsigned char> bicubicresize(const std::vector<unsigned char>& inDecodedImageData,
										 std::size_t src_width,
										 std::size_t src_height,
										 std::size_t dest_width,
										 std::size_t dest_height,
										 int channels);

std::vector<unsigned char> rgb2rgba(const std::vector<unsigned char>& data);

} } }

#endif
