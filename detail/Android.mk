LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := bicubicresize_static
LOCAL_MODULE_FILENAME := libbicubicresize

LOCAL_SRC_FILES := bicubicFunc.cpp 	

LOCAL_C_INCLUDES := $(LOCAL_PATH)

LOCAL_CFLAGS := -ftree-vectorize -fomit-frame-pointer -funroll-loops -ffast-math -Ofast -std=c++11

include $(BUILD_STATIC_LIBRARY)		
