LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := vidxtream_platform_static
LOCAL_MODULE_FILENAME := libvidxtream_platform

LOCAL_SRC_FILES := \
	android/Application_extensions_Android.cpp \
	android/DialogsAndSheets_Android.cpp \
	android/NativeVideoPlayer_Android.cpp \
	android/OpenGLContext_Android.cpp \
	android/ParallaxManager_Android.cpp \
	ImageFileResizer.cpp
	

LOCAL_C_INCLUDES := $(LOCAL_PATH) \
					$(LOCAL_PATH)/detail \
					$(LOCAL_PATH)/android 
					
					# $(LOCAL_PATH)/android/details 

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)

LOCAL_LDLIBS := -lGLESv2 -lEGL
LOCAL_EXPORT_LDLIBS := -lGLESv2 -lEGL

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static
LOCAL_WHOLE_STATIC_LIBRARIES += bicubicresize_static
LOCAL_WHOLE_STATIC_LIBRARIES += filesystem_static

include $(BUILD_STATIC_LIBRARY)		

$(call import-module,cocos2dx)
$(call import-module,platform/detail)
$(call import-module,filesystem-utils)