//
//  NativeVideoPlayer.h
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#ifndef __POV2__NativeVideoPlayer__
#define __POV2__NativeVideoPlayer__

#include <string>
#include <functional>


#define kNativeVideoPlayerDidFinishNotification rad::NativeVideoPlayer::kDidFinishNotification

namespace rad { namespace NativeVideoPlayer {

	/**
	 *	Posted to the shared CCNotificationCenter upon completion of a video, prior to releasing control of the UI back to the application.
	 *	The path to the video file will be passed as the object to the notication handler.
	 */
	extern const char * kDidFinishNotification;
	
	/**
	 *  Play a video file at 'path' with the native video player interface.
	 *	The interface will take over the UI and play the movie.
	 *	Upon completion, the callback will be called and the
	 *	kNativeVideoPlayerDidFinishNotification will be posted
	 *	to the shared CCNotificationCenter with the video path
	 *	passed as the object.
	 *
	 *  @param path         The full path to the video file to be played.
	 *  @param completion A callback that will be called when the native interface is dismissed, prior to releasing control of the UI back to the application.
	 *	The path to the video will be passed to the callback.
	 */
	void playVideo(const std::string& path, std::function<void (const std::string&)> completion);
	
} }

#endif /* defined(__POV2__NativeVideoPlayer__) */
