//
//  Application_extensions.h
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#ifndef __POV2__Application_extensions__
#define __POV2__Application_extensions__

#include <string>

namespace rad { namespace Application_extensions {
	
	/**
	 *  Opens the resource at the specified URL.
	 *
	 *  @param url An object representing a URL (Universal Resource Locator). On iOS, UIKit supports many schemes, including http, https, tel, facetime, and mailto schemes.
	 *
	 *  @return true if the resource located by the URL was successfully opened; otherwise false.
	 */
	bool openURL(const std::string& url);
	
	/**
	 *  Returns whether an app can open a given URL resource.
	 *
	 *  @param url A URL object that identifies a given resource. The URL’s scheme—possibly a custom scheme—identifies which app can handle the URL.
	 *
	 *  @return false if no app is available that will accept the URL; otherwise, returns true.
	 *	
	 *	@discussion This method guarantees that that if openURL() is called, another app will be launched to handle it. It does not guarantee that the full URL is valid.
	 */
	bool canOpenURL(const std::string& url);
	
	/**
	 *  Displays a sharing interface to the user that allows them to share the item located at the given url.
	 *
	 *  @param url A URL object that identifies a given resource.
	 *	
	 *	@discussion On iOS, this creates and shows an instance of UIActivityViewController to share the item at url.
	 *				On Android, this method would use intents to query other applications that might handle the url.
	 */
	void shareURL(const std::string& url);
	
	/**
	 *  Returns the location of discardable cache files (Library/Caches).
	 *
	 *  @return Location of discardable cache files (Library/Caches).
	 */
	std::string getCacheDirectory();
	
} }

#endif /* defined(__POV2__Application_extensions__) */
