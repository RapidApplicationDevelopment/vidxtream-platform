//
//  ImageFileResizer.h
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#ifndef __POV2__ImageFileResizer__
#define __POV2__ImageFileResizer__

#include <string>

namespace rad { namespace plat {
	
	/**
	 *  Reads an image from a file, resizes it to the given dimensions while maintaining the image's aspect ratio, and writes the resized image to a destination file.
	 *
	 *	@class ImageFileResizer
	 */
	class ImageFileResizer {
	public:
		/**
		 *  A container object for the dimensions of the output image.
		 */
		struct Size {
			
			float width, height;
			
			Size(float w, float h)
			: width(w)
			, height(h)
			{}
			Size()
			: Size(-1.f,-1.f)
			{}
		};
		
	private:
		std::string _srcFile, _dstFile;
		Size _size;
		bool size_set;
		
	public:
		
		/**
		 *  Helper function to generate a size that maintains the aspect ratio and will fit on the native graphics hardware.
		 *
		 *  @param width  The width
		 *  @param height The height
		 *
		 *  @return A Size object that fits on the native graphics hardware.
		 *
		 *	@discussion If width or height is greater than the largest texture that is supported by the graphics hardware, then the returned size will be a scaled so that the largest dimension just fits while maintaining the original width/height aspect ratio.
		 */
		static
		Size sizeToFitInTexture(const float width, const float height);
	public:
		
		~ImageFileResizer() {}
		
		ImageFileResizer()
		: size_set(false)
		{}
		
		/**
		 *  Sets the path to the image that will be resized.
		 *
		 *  @param src The path to the image that will be resized.
		 */
		void setSource(const std::string& src)
		{
			this->_srcFile = src;
		}
		
		/**
		 *  Sets the full path and filename where the image should be written after being resized.
		 *
		 *  @param dest Full path where the resized image should be written.
		 */
		void setDestination(const std::string& dest)
		{
			this->_dstFile = dest;
		}
		
		/**
		 *  Sets the destination width and height.  Also sets a flag indicating that the _size variable has been initialized (for internal use).
		 *
		 *  @param w A floating point number for the width of the image when it is resized.
		 *  @param h A floating point number for the height of the image when it is resized.
		 */
		void setSize(float w, float h) {
			this->_size.width = w;
			this->_size.height = h;
			this->size_set = true;
		}
		
		/**
		 *  Performs the resize operation.  This will read the image from _srcFile, resize it to _size, and write it to _dstFile.
		 *
		 *  @return true if the src image was successfully resized and written to dst; otherwise false.
		 */
		bool resize();
		
	private:
		
		/**
		 *  Reads the image located at src, resizes it to be of size, and writes to dst.
		 *
		 *  @param src      Full file path for the source image.
		 *  @param dst      Full file path where the resized image should be written.
		 *  @param size     The resulting size of the image when it is written to dst.
		 *  @param size_set If true, use the size parameter; otherwise, resize the image to be the largest that will fit onto the graphics hardware.
		 *
		 *  @return true if the src image was successfully resized and written to dst; otherwise false.
		 *	
		 *	@discussion		This is where 'platform specific' may come into play.
		 *					My initial plan for the Java impl are as follows:
		 *						-	Load the image using BitmapFactory.decodeFile( src, \options\ ) passing size as an option
		 *						-	Resize the image using Bitmap.createScaledBitmap()
		 *						-	Given the destination path extension, determine output Bitmap.CompressFormat (JPG, PNG)
		 *						-	Open an Outputstream to the destination path using new BufferedOutputStream(new FileOutputStream( destination ))
		 *						-	Write the Bitmap to the outputstream using Bitmap.compress( fmt, quality, outputStream )
		 *
		 *
		 *					The concern is with the bitmap scaling.  We want to maintain the aspect ratio (so uniform scaling is necessary) and be able to show the user the image in it's entirety (so no cropping).
		 *					Ultimately, we can probably implement this with a Bilinear filter or maybe sws_scale()
		 */
		static bool readResizeWrite(const std::string& src, const std::string& dst, const Size& size, const bool size_set);
	};
	
} };

#endif /* defined(__POV2__ImageFileResizer__) */
