//
//  DialogsAndSheets.h
//  POV2
//
//  Created by Clayton Thomas on 6/11/14.
//
//

#ifndef __POV2__DialogsAndSheets__
#define __POV2__DialogsAndSheets__

#include <vector>
#include <string>
#include <memory>
namespace rad {
	
	/**
	 *  Defines the protocol for delegates of an indexed dialog.
	 */
	class IndexedDialogDelegate {
	public:
		virtual ~IndexedDialogDelegate();
		
		/**
		 *  Called upon dismissal of a UI dialog that was presented to the user.
		 *
		 *  @param index The index of the item selected by the user.  0 corresponds to 'Cancel'.
		 *  @param msg1  Optional String containing the text entered by the user (if a text field was present).
		 *  @param msg2  Optional String containing the text entered by the user (if a secondary text field was present).
		 */
		virtual void clickedButtonAtIndex(int index, const char *msg1 = nullptr, const char *msg2 = nullptr) =0;
	};
	
	inline IndexedDialogDelegate::~IndexedDialogDelegate() {}
	
	typedef std::shared_ptr<IndexedDialogDelegate> IndexedDialogDelegateRef;
	
	
	/**
	 *  Style enumeration for the type of ConfirmationBox to present to the user.
	 */
	enum ConfirmationBoxStyle {
		/// A standard alert.
		DEFAULT = 0,
		/// An alert that allows the user to enter text. The text field is obscured.
		SECURE,
		/// An alert that allows the user to enter text.
		PLAIN,
		/// An alert that allows the user to enter a login identifier and password.
		LOGIN_AND_PASS,
		/// Invalid style sentinel
		INVALID
	};
	
	/**
	 *  Presents a popup dialog to the user.  Note: The number of text fields present in an alert is dependent on the style of the alert.
	 *
	 *  @param pszTitle          The string that appears in the receiver’s title bar.
	 *  @param pszMsg            Descriptive text that provides more details than the title.
	 *  @param delegate          The receiver’s delegate or nullptr if it doesn’t have a delegate.
	 *  @param "Cancel"          The title of the cancel button or nullptr if there is no cancel button. Index 0.
	 *  @param "OK"              The title of the confirm button or nullptr if there is no confirm button. Index 1.
	 *  @param boxType           The kind of alert displayed to the user.
	 *  @param textFieldDefault	 The placeholder/default text for the first text field, if present.
	 *  @param textFieldDefault2 The placeholder/default text for the second text field, if present.
	 */
	void CCConfirmationBox(const char * pszTitle, const char *  pszMsg, IndexedDialogDelegateRef delegate, const char * pszCancelBtnTitle = "Cancel", const char * pszConfirmBtnTitle = "OK", ConfirmationBoxStyle boxType = ConfirmationBoxStyle::DEFAULT, const char * textFieldDefault = nullptr, const char * textFieldDefault2 = nullptr);
	
	/**
	 *  Initializes the action sheet using the specified starting parameters and displays the sheet to the user.
	 *
	 *  @param title             A string to display in the title area of the action sheet. Pass nullptr if you do not want to display any text in the title area.
	 *  @param delegate          The receiver’s delegate object. Although this parameter may be nullptr, the delegate is used to respond to taps in the action sheet and should usually be provided.
	 *  @param cancelButton      The title of the cancel button. This button is added to the action sheet automatically and assigned the index 0. This button is displayed in black to indicate that it represents the cancel action. Specify nullptr if you do not want a cancel button or are presenting the action sheet on an iPad.
	 *  @param destructiveButton The title of the destructive button. This button is added to the action sheet automatically and assigned the index 1. This button is displayed in red to indicate that it represents a destructive behavior. Specify nullptr if you do not want a destructive button.
	 *  @param otherButtons      The titles of any additional buttons you want to add.
	 */
	void CCActionSheet(const char * title, IndexedDialogDelegateRef delegate, const char * cancelButton, const char * destructiveButton, const std::vector<std::string>& otherButtons);
}

#endif /* defined(__POV2__DialogsAndSheets__) */
