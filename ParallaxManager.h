//
//  ParallaxManager.h
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#ifndef __POV2__ParallaxManager__
#define __POV2__ParallaxManager__

namespace rad
{
	class ParallaxManager {
		ParallaxManager() =delete;
		~ParallaxManager() =delete;
	public:
		static bool deviceMotionAvailable();
		static bool startUpdating();
		static bool stopUpdating();
		static bool setUpdateInterval(double i);
	protected:
		/* implemented elsewhere */
			// must be run on main thread
		static void updateMotion(float x, float y);
		static void updateMotionFromBackgroundThread(float x, float y);
	};
}

#endif /* defined(__POV2__ParallaxManager__) */
