//
//  ImageFileResizer.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#include "ImageFileResizer.h"
#include "CCConfiguration.h"
#include "platform/CCFileUtils.h"
#include "platform/CCImage.h"


#if defined DEBUG_IMAGE_FILE_RESIZER && defined DEBUG && DEBUG
#include "platform/CCCommon.h"
#define ErrLog(format, ...) CCLOG(format, ##__VA_ARGS__)
#else
#define ErrLog(format, ...)
#endif

//#define RESIZE_BY_TILING

#ifdef RESIZE_BY_TILING

#	include "misc_nodes/CCRenderTexture.h"
#	include "textures/CCTexture2D.h"
#	include "textures/CCTextureCache.h"

#else // RESIZE_BY_TILING

#	include "detail/bicubicFunc.h"

#endif // RESIZE_BY_TILING


extern template class std::vector<unsigned char>;

namespace
{
#ifdef RESIZE_BY_TILING
	
	enum QuadPosition {
		TOP_LEFT,
		TOP_RIGHT,
		BOTTOM_LEFT,
		BOTTOM_RIGHT
	};
	
	// inputs: source image, destination array, desired quadrant
	// quadrants: 0 = top left, 1 = top right, 2 = bottom left, 3 = bottom right
	void getQuad(cocos2d::CCImage *image, unsigned char *&tempArr, const QuadPosition quad){
		unsigned char *sourcePtr = image->getData();
		int bytesPerPixel;
		if (image->hasAlpha()) {
			bytesPerPixel = 4;
		}
		else {
			bytesPerPixel = 3;
		}
		switch (quad) {
			default:
			case TOP_LEFT:
				break;
			case TOP_RIGHT:
				sourcePtr += (bytesPerPixel * image->getWidth()/2);
				break;
			case BOTTOM_LEFT:
				sourcePtr += (bytesPerPixel * image->getHeight() * image->getWidth() / 2);
				break;
			case BOTTOM_RIGHT:
				sourcePtr += (bytesPerPixel * ((image->getHeight() * image->getWidth() / 2) + image->getWidth() / 2));
				break;
		}
		const int ho2 = image->getHeight()/2;
		const int wo2xbpp = bytesPerPixel*(image->getWidth()/2);
		for (int i = 0; i < ho2; i++) {
			memcpy(tempArr + (i * wo2xbpp), sourcePtr, wo2xbpp);
			sourcePtr += wo2xbpp+wo2xbpp;
		}
	}
	
	cocos2d::CCRenderTexture* newTileImage(cocos2d::CCImage *image, float rtWidth, float rtHeight) {
		cocos2d::CCRenderTexture *rt;
		
		const unsigned int width = image->getWidth()/2, height = image->getHeight()/2;

		

		const cocos2d::CCTexture2DPixelFormat pixfmt = (image->hasAlpha()?cocos2d::kCCTexture2DPixelFormat_RGBA8888:cocos2d::kCCTexture2DPixelFormat_RGB888);
		const cocos2d::CCSize dataSize(width,height);
		
		rt = new cocos2d::CCRenderTexture();
		
		cocos2d::CCRect temp(0.0f, 0.0f, rtWidth/2, rtHeight/2);
		
		rt->initWithWidthAndHeight(rtWidth, rtHeight, cocos2d::kCCTexture2DPixelFormat_RGBA8888, 0);
		
		rt->beginWithClear(0.0f, 0.0f, 0.0f, 1.0f);
		{
			unsigned char *tempArr = (new unsigned char[(width*height*(image->hasAlpha()?4:3))]);
	
			CCLOG("drawing top left");
			{
				// top left texture
				temp.origin.setPoint(0.0f, temp.size.height);
				
				getQuad(image, tempArr, QuadPosition::TOP_LEFT);
				cocos2d::CCTexture2D *topLeft = new cocos2d::CCTexture2D;
				topLeft->initWithData(tempArr, pixfmt, width, height, dataSize);
				
				topLeft->drawInRect(temp);
				
				topLeft->release();
			}
			CCLOG("drawing top right");
			{
				// top right texture
				temp.origin.setPoint(temp.size.width, temp.size.height);
				
				getQuad(image, tempArr, QuadPosition::TOP_RIGHT);
				cocos2d::CCTexture2D *topRight = new cocos2d::CCTexture2D;
				topRight->initWithData(tempArr, pixfmt, width, height, dataSize);
				
				topRight->drawInRect(temp);
				
				topRight->release();
			}
			CCLOG("drawing bottom left");
			{
				// bottom left texture
				temp.origin.setPoint(0.0f, 0.0f);
				
				getQuad(image, tempArr, QuadPosition::BOTTOM_LEFT);
				cocos2d::CCTexture2D *botLeft = new cocos2d::CCTexture2D;
				botLeft->initWithData(tempArr, pixfmt, width, height, dataSize);
				
				botLeft->drawInRect(temp);
				
				botLeft->release();
			}
			CCLOG("drawing bottom right");
			{
				// bottom right texture
				temp.origin.setPoint(temp.size.height, 0.0f);
				
				getQuad(image, tempArr, QuadPosition::BOTTOM_RIGHT);
				cocos2d::CCTexture2D *botRight = new cocos2d::CCTexture2D;
				botRight->initWithData(tempArr, pixfmt, width, height, dataSize);
				
				botRight->drawInRect(temp);
				
				botRight->release();
			}
			delete [] tempArr;
			tempArr = nullptr;
		}
		rt->end();
		CCLOG("end");
		return rt;
	}
	
#endif
	
}

namespace rad { namespace plat {

	ImageFileResizer::Size ImageFileResizer::sizeToFitInTexture(const float width, const float height)
	{
		Size newSize(width, height);
			// Let's cap the max size at 2048, for devices that support 4096x4096 textures (which is unnecessary)
		const auto max_size = std::min(2048.0f, (float)cocos2d::CCConfiguration::sharedConfiguration()->getMaxTextureSize());

		if (width > max_size || height > max_size)
		{
			const float ws = max_size/width;
			const float hs = max_size/height;
			const float scale = std::min(ws, hs);
			newSize.height *= scale;
			newSize.width *= scale;
		}
		
		return std::move(newSize);
	}
	
	
	/* platform specific ImageFileResizer::readResizeWrite() called here */
	bool ImageFileResizer::resize()
	{
		const auto& srcFile = this->_srcFile;
		const auto& dstFile = this->_dstFile;
		ErrLog("src = %s, dst = %s",srcFile.c_str(), dstFile.c_str());
		if (dstFile.empty() == false &&
			srcFile.empty() == false &&
			cocos2d::CCFileUtils::sharedFileUtils()->isFileExist(srcFile))
		{
			ErrLog("srcFileExists... starting resize");
			return ImageFileResizer::readResizeWrite(srcFile, dstFile, this->_size, this->size_set);
		}
		return false;
	}
	
	bool ImageFileResizer::readResizeWrite(const std::string& src, const std::string& dst, const Size& dst_size, const bool size_set)
	{
		Size size(dst_size.width,dst_size.height);
		cocos2d::CCImage::EImageFormat srcFmt = cocos2d::CCImage::EImageFormat::kFmtPng, dstFmt = cocos2d::CCImage::EImageFormat::kFmtRawData;
		cocos2d::CCImage *srcImage, dstImage;
	
		
		
		if (std::string::npos == src.rfind(".png")) {
			srcFmt = cocos2d::CCImage::EImageFormat::kFmtJpg;
		}
		
		srcImage = new cocos2d::CCImage();
		srcImage->initWithImageFileThreadSafe(src.c_str(), srcFmt);
		ErrLog("srcImage size = %dx%d",srcImage->getWidth(), srcImage->getHeight());
		
		if (size_set == false) {
			size = ImageFileResizer::sizeToFitInTexture(static_cast<const float>(srcImage->getWidth()), static_cast<const float>(srcImage->getHeight()));
		}
		
		ErrLog("resizing to %fx%f",size.width,size.height);
		
		if (srcImage->getWidth() < size.width || srcImage->getHeight() < size.height) {
			ErrLog("srcImage size ?smaller? than size to resize = %fx%f... I don't think that we should be using this method to make an image larger...",size.width,size.height);
			srcImage->autorelease();
			return srcImage->saveToFile(dst.c_str());
		}
		
#ifdef RESIZE_BY_TILING
		cocos2d::CCRenderTexture* rt = newTileImage(srcImage, dst_size.width, dst_size.height);
		CCLOG("return");
		srcImage->release();
		CCLOG("after nulling texture");
		cocos2d::CCImage *newimage = rt->newCCImage(true);
		CCLOG("releasing rt");
		rt->release();
		
		CCLOG("adding to TextureCache");
		const bool ret = nullptr != cocos2d::CCTextureCache::sharedTextureCache()->addUIImage(newimage, dst.c_str());
		newimage->release();
		return ret;
#else
		
		const bool hasAlpha = srcImage->hasAlpha();
		
		const std::size_t bitsPerPixel = hasAlpha ? 32 : 24;

		const std::size_t bytesPerPixel = bitsPerPixel/8;
		const std::size_t width = srcImage->getWidth();
		const std::size_t height = srcImage->getHeight();
		const std::size_t bytesPerRow = bytesPerPixel * width;
		const std::size_t dataLength = bytesPerRow * height;
		const std::size_t dwidth(size.width);
		const std::size_t dheight(size.height);
		const int dDataLength = 4 * int(dwidth * dheight);

		ErrLog("copying data of length = %ld",dataLength);

		std::vector<unsigned char> inDecodedImageData(srcImage->getData(), srcImage->getData() + dataLength);
		ErrLog("freeing up srcImage memory");
		srcImage->release();
		ErrLog("bicubicresizing: channels = %lu",bytesPerPixel);
		
		std::vector<unsigned char> result(detail::bicubicresize(inDecodedImageData, width, height, dwidth, dheight, static_cast<int>(bytesPerPixel)));
		ErrLog("finished resize... checking it worked");

		ErrLog("freeing up memory");
		// free up inDecodedImageData memory
		inDecodedImageData.clear();
		
		if (bytesPerPixel == 3) {
			ErrLog("rgb2rgba");
			result = detail::rgb2rgba(result);
		}
		
		ErrLog("dstImage initWithImageData");
		dstImage.initWithImageData((void *)result.data(), dDataLength, dstFmt, dwidth, dheight, 8);
		ErrLog("freeing up result memory");
		// free up this memory
		result.clear();
		ErrLog("saving to file");
		return dstImage.saveToFile(dst.c_str(), true);
#endif
	}
} }

