# This is the platform dependent code for the __VidXtream__, __POV2__, & __Piper__ applications
---
I use this library as a reference when showing interns or external developers how to access native code through the JNI interface on Android (e.g., libav, opencv, opengles, etc).
The efferent connections are shown in `Android.mk`.

#### Note: java/ directory contains example code for platform specific usage and implementation

### OpenGLContext
---
A Wrapper for the native OpenGL context. When using the gpu from a background thread, we need to use a separate, but shared, OpenGL context. This class can be used to switch back-and-forth between multiple contexts, see `rad::OpenGLContext::setCurrentContext` and `rad::OpenGLContext::makeCurrent`, in a platform-specific way.


### DialogsAndSheets
---
Display system popups and menu selections with variable input. A delegate is provided callback information about which inputs were selected.



### ImageFileResizer
---
Images downloaded from an action camera can often be larger than the texture limit of the devices GPU. This class resizes image files so that they may fit inside a single texture without tiling.
#### Note: This class is now fully implemented in native code. See the documentation of `rad::plat::ImageFileResizer::readResizeWrite` for more information.



### NativeVideoPlayer
---
Display videos recorded on the action camera in the system video player. Caller may provide a completion function and/or register for `rad::NativeVideoPlayer::kDidFinishNotification` notifications.



### Application_Extensions
---
Access or display content from other applications


### ParallaxManager
---
Encapsulates the code to handle parallax movement of UI elements, based on "z-distance" from the user. As this was a new iOS feature at the time, the Android code contains stubs for future development.


