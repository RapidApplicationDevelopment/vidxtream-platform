//
//  OpenGLContext.h
//  MacH264InDisplay
//
//  Created by Clayton Thomas on 5/2/14.
//
//

#ifndef __MacH264InDisplay__OpenGLContext__
#define __MacH264InDisplay__OpenGLContext__

#include <memory>
namespace rad
{
	/**
	 *  Wrapper for the native graphics context.
	 *	
	 *	@class OpenGLContext
	 *	@discussion Wraps each platform's native OpenGL context.
	 *	Also contains a helper method to initialize a shared context using the main cocos2d context.
	 *	This is mostly used for multithreading GL calls.
	 *
	 */
	class OpenGLContext
	{
		class Impl;
		std::unique_ptr<Impl> impl;
	public:
		/**
		 *  If this is the current context bound in the current thread, then the destructor will set the current context to nullptr.  See the discussion for OpenGLContext::setCurrentContext().
		 */
		~OpenGLContext();
		
		/**
		 *  Creates and initializes the native handle. 
		 */
		OpenGLContext();
		
		/**
		 *  Initializes a new GL context.
		 *	Equivalent to passing nullptr to initWithSharedContext().
		 *	
		 *	@discussion To issue OpenGL ES commands to this context, you must first make it the current drawing context by calling makeCurrent
		 *
		 *  @return true on success, false on failure.
		 */
		bool init();
		
		/**
		 *  Initializes a new GL context that shares state with the passed context.
		 *
		 *	@discussion To issue OpenGL ES commands to this context, you must first make it the current drawing context by calling makeCurrent
		 *
		 *  @param shared The context with which to share state.
		 *		Passing nullptr is equivalent to calling init().
		 *
		 *
		 *  @return true on success, false on failure.
		 */
		bool initWithSharedContext(OpenGLContext *shared);
		
		/**
		 *  Initializes a new GL context that shares state with main cocos2d-x context.
		 *	
		 *	@discussion To issue OpenGL ES commands to this context, you must first make it the current drawing context by calling makeCurrent
		 *
		 *  @return true on success, false on failure.
		 */
		bool initWithCocos2dAsSharedContext();
		
		/**
		 *  Whether or not this is the bound context for the current thread.
		 *
		 *  @return true if this context is currently bound on the current thread, false otherwise.
		 */
		bool isCurrent() const;
		
		/**
		 *  Makes the specified context the current rendering context for the calling thread.
		 *	
		 *	@discussion If this context is not the current context, then you should call glflush() and glbind*() before making it current.
		 *
		 *  @return true if successful; otherwise, false. If an error occurred, the rendering context for the current thread remains unchanged.
		 */
		bool makeCurrent();
		
		/**
		 *  Makes the specified context the current rendering context for the calling thread.
		 *	
		 *	@discussion A context encapsulates all OpenGL ES state for a single thread in your app. When you call any OpenGL ES API function, OpenGL ES evaluates it with respect to the calling thread’s current context. Because OpenGL ES functions require a current context, you must use this method to select a context for the current thread before calling any OpenGL ES function. Unless otherwise specified, OpenGL ES commands calls made in the same context complete in the order they are called.
		 *
		 *	OpenGL ES retains the context when it is made current, and it releases the previous context. Calling this method with a nil parameter releases the current context and leaves OpenGL ES unbound to any drawing context.
		 *
		 *	You should avoid making the same context current on multiple threads. OpenGL ES provides no thread safety, so if you want to use the same context on multiple threads, you must employ some form of thread synchronization to prevent simultaneous access to the same context from multiple threads.		 
		 *
		 *  @param context The rendering context that you want to make current.
		 *
		 *  @return true if successful; otherwise, false. If an error occurred, the rendering context for the current thread remains unchanged.
		 */
		static bool setCurrentContext(OpenGLContext* context);
	};
}

#endif /* defined(__MacH264InDisplay__OpenGLContext__) */
