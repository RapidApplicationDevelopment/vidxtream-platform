//
//  ParallaxManager_Android.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#include "../ParallaxManager.h"
#include "details/NDKHelperObjects.h"
#include "jni/JniHelper.h"
#include <cstdlib>
#include <jni.h>
#include <android/log.h>

#define CLASS_NAME "net/rapidappdevel/pov2/POV2"
using namespace cocos2d;
namespace rad
{
	bool ParallaxManager::deviceMotionAvailable()
	{
		return false;
	}
	
	bool ParallaxManager::startUpdating()
	{
        /*
			start updating the devices pitch and roll
        */

		bool result = ParallaxManager::deviceMotionAvailable();
		if (result)
		{
			JniMethodInfo t;
			if (JniHelper::getStaticMethodInfo(
													t,
													CLASS_NAME,
													"startUpdating",
													NDK_ARG_TYPES NDK_RETURN_TYPE NDK_BOOL_TYPE
													)) {
				result = t.env->CallStaticBooleanMethod(t.classID, t.methodID);
				t.env->DeleteLocalRef(t.classID);
			}
			else
				result = false;
		}
		return result;
	}
	bool ParallaxManager::stopUpdating()
	{
		bool result = ParallaxManager::deviceMotionAvailable();
		if (result)
		{
			JniMethodInfo t;
			if (JniHelper::getStaticMethodInfo(
													t,
													CLASS_NAME,
													"stopUpdating",
													NDK_ARG_TYPES NDK_RETURN_TYPE NDK_BOOL_TYPE
													)) {
				result = t.env->CallStaticBooleanMethod(t.classID, t.methodID);
				t.env->DeleteLocalRef(t.classID);
			}
			else
				result = false;
		}

		return result;
	}
	bool ParallaxManager::setUpdateInterval(double i)
	{
		bool result = ParallaxManager::deviceMotionAvailable();
		if (result)
		{
			JniMethodInfo t;
			if (JniHelper::getStaticMethodInfo(
													t,
													CLASS_NAME,
													"startUpdating",
													NDK_ARG_TYPES NDK_DOUBLE_TYPE NDK_RETURN_TYPE NDK_BOOL_TYPE
													)) {
				result = t.env->CallStaticBooleanMethod(t.classID, t.methodID, i);
				t.env->DeleteLocalRef(t.classID);
			}
			else
				result = false;
		}
		return result;
	}
	
}
