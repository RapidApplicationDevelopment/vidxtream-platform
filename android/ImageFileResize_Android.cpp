//
//  ImageFileResize_Android.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#include "../ImageFileResizer.h"
/*
#include "details/NDKHelperObjects.h"
#include <cstdlib>
#include <jni.h>
#include "jni/JniHelper.h"

#define CLASS_NAME "net/rapidappdevel/pov2/POV2"

using namespace cocos2d;

static 	jstring createJString(JNIEnv*  env, const std::string& str) {
		if (str.empty()) {
			return env->NewStringUTF("");
		} else {
			return env->NewStringUTF(str.c_str());
		}
	}

namespace rad { namespace plat {
	
	bool ImageFileResizer::readResizeWrite(const std::string& srcFile, const std::string& dstFile, const Size& size, const bool size_set)
	{
		JniMethodInfo t;
		bool result = false;
		float height = size.height;
		float width = size.width;

		if (JniHelper::getStaticMethodInfo(
											t,
											CLASS_NAME,
											"readResizeWrite",
											NDK_ARG_TYPES NDK_STRING_TYPE NDK_STRING_TYPE NDK_FLOAT_TYPE NDK_FLOAT_TYPE NDK_RETURN_TYPE NDK_BOOL_TYPE
											)){
			jstring source, dest;
			source = createJString(t.env, srcFile);
			dest = createJString(t.env, dstFile);
			result = t.env->CallStaticBooleanMethod(t.classID, t.methodID, source, dest, width, height);

			t.env->DeleteLocalRef(source);
			t.env->DeleteLocalRef(dest);
			t.env->DeleteLocalRef(t.classID);
			
		}
		return result;
	}
	
} }
*/
