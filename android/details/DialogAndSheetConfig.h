//
//  DialogAndSheetConfig.h
//  
//
//  Created by Clayton Thomas on 6/19/14.
//
//

#ifndef _DialogAndSheetConfig_h
#define _DialogAndSheetConfig_h


#define CLASS_NAME "net/rapidappdevel/DialogHelper"

#define CLASS_PROTO net_rapidappdevel_DialogHelper

#define RAD_JAVA_CONFIRMBOX_FUNCNAME "CreatingDialogBox"
#define RAD_JAVA_ACTIONSHEET_FUNCNAME "showActionSheet"


#define NDK_CONFIRMBOX_CALL_MARSHALLING(title, message, cancelButton, confirmButton, style, textField1, textField2) \
title, message, cancelButton, confirmButton, style, textField1, textField2

#define NDK_ACTIONSHEET_CALL_MARSHALLING(title, cancelButton, confirmButton, otherButtons) \
title, cancelButton, confirmButton, otherButtons


#define RAD_JNI_FUNCNAME(name, ...) Java_##CLASS_PROTO##_##name(JNIEnv*  env, jobject thiz, ##__VA_ARGS__)


#endif
