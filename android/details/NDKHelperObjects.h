//
//  NDKHelperObjects.h
//  
//
//  Created by Clayton Thomas on 6/19/14.
//
//

#ifndef _NDKHelperObjects_h
#define _NDKHelperObjects_h

#define NDK_OBJECT_TYPE "Ljava/lang/Object;"
#define NDK_STRING_TYPE "Ljava/lang/String;"
#define NDK_ARRAYLIST_TYPE "Ljava/util/ArrayList;"
#define NDK_INT_TYPE "I"
#define NDK_BOOL_TYPE "Z"
#define NDK_BYTE_TYPE "B"
#define NDK_CHAR_TYPE "C"
#define NDK_SHORT_TYPE "S"
#define NDK_LONG_TYPE "J"
#define NDK_FLOAT_TYPE "F"
#define NDK_DOUBLE_TYPE "D"
#define NDK_VOID_TYPE "V"

#define NDK_ARG_TYPES "("
#define NDK_RETURN_TYPE ")"

#define NDK_DEFAULT_CONSTRUCTOR_NAME "<init>"
#define NDK_DEFAULT_CONSTRUCTOR_SIGNATURE "()V"

#define NDK_FUNCTION_MANGLER(class_name, name) Java_ ## class_name ## name

#define NDK_ARRAYLIST_CLASS "java/util/ArrayList"

/*	A simple RAII example. 
	Since most jni objects seem to 'inherit' from jobject, the template parameter may not be necessary.

namespace rad { namespace jni {
	
	template <typename JNIType>
	class scoped_object {
		
		JNIType& obj;
		JNIEnv& env;
		
	public:
		~scoped_object() {
			this->env.DeleteLocalRef(this->obj);
		}
		
		scoped_object(JNIType& o, JNIEnv& envir)
		: obj(o)
		, env(envir)
		{}

		operator JNIType&() {
			return this->obj;
		}
		
			// not all compilers auto generate a const version
		operator const JNIType&() const {
			return this->obj;
		}
		
	private:
		scoped_object(scoped_object&) =delete;
		scoped_object& operator=(scoped_object&) =delete;
	};

} }
*/

#endif