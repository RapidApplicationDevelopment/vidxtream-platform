//
//  DialogsAndSheets.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/11/14.
//
//

#include "../DialogsAndSheets.h"
#include <cstdlib>
#include <jni.h>
#include <android/log.h>
#include <string>
#include "jni/JniHelper.h"
#include <cassert>
#include <stack>
#include <vector>

#include "details/DialogAndSheetConfig.h"
#include "details/NDKHelperObjects.h"

#define  LOG_TAG    "Java_net_rapidappdevel_DialogHelper.cpp"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)


using namespace cocos2d;

namespace
{
	std::stack<rad::IndexedDialogDelegateRef> pDelegates;
	
	void pushDelegate(rad::IndexedDialogDelegateRef delegate) {
		pDelegates.push(delegate);
	}
	
	rad::IndexedDialogDelegateRef getCurrentDelegate() {
		assert(false == pDelegates.empty());
		return pDelegates.top();
	}
	
	void popDelegate() {
		auto delegate = getCurrentDelegate();
		pDelegates.pop();
		
	}
	
	
	jstring createJString(JNIEnv*  env, const char *str) {
		if (!str) {
			return env->NewStringUTF("");
		} else {
			return env->NewStringUTF(str);
		}
	}
	
	jobject convertVectorToArrayList(JNIEnv*  env, const std::vector<std::string>& vec) {
		
		jclass clazz = env->FindClass(NDK_ARRAYLIST_CLASS); // do I need to drop the ';' here?

		jobject obj = env->NewObject(clazz, env->GetMethodID(clazz, NDK_DEFAULT_CONSTRUCTOR_NAME, NDK_DEFAULT_CONSTRUCTOR_SIGNATURE));
		jmethodID arrayListAdd = env->GetMethodID(clazz, "add", NDK_ARG_TYPES NDK_OBJECT_TYPE NDK_RETURN_TYPE NDK_BOOL_TYPE);

		for (const auto &stlstr : vec)
		{
			auto str = stlstr.c_str();
			jstring _str = env->NewStringUTF(str);
			env->CallBooleanMethod(obj, arrayListAdd, _str);
		}
		
		return obj;
	}
}

namespace rad
{
	void CCConfirmationBox(const char * pszTitle, const char *  pszMsg, IndexedDialogDelegateRef delegate, const char * pszCancelBtnTitle, const char * pszConfirmBtnTitle, ConfirmationBoxStyle boxType, const char * textFieldDefault, const char * textFieldDefault2)
	{
		JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(
										   t,
										   CLASS_NAME,
										   RAD_JAVA_CONFIRMBOX_FUNCNAME,
										   NDK_ARG_TYPES NDK_STRING_TYPE NDK_STRING_TYPE NDK_STRING_TYPE NDK_STRING_TYPE NDK_INT_TYPE NDK_STRING_TYPE NDK_STRING_TYPE NDK_RETURN_TYPE NDK_VOID_TYPE
										   ))
		{
			
			jstring title, message, cancelButton, confirmButton, textField1, textField2;
			int style;
			
			pushDelegate(delegate);
			
			style = static_cast<int>(boxType);
			
			title = createJString(t.env, pszTitle);
			message = createJString(t.env, pszMsg);
			cancelButton = createJString(t.env,pszCancelBtnTitle);
			confirmButton = createJString(t.env, pszConfirmBtnTitle);
			textField1 = createJString(t.env, textFieldDefault);
			textField2 = createJString(t.env, textFieldDefault2);
			LOGD("create confirmation, now calling: %s   %s",pszTitle, pszMsg);
			t.env->CallStaticVoidMethod(t.classID, t.methodID, NDK_CONFIRMBOX_CALL_MARSHALLING(title, message, cancelButton, confirmButton, style, textField1, textField2));
			
			t.env->DeleteLocalRef(title);
			t.env->DeleteLocalRef(message);
			t.env->DeleteLocalRef(cancelButton);
			t.env->DeleteLocalRef(confirmButton);
			t.env->DeleteLocalRef(textField1);
			t.env->DeleteLocalRef(textField2);
			t.env->DeleteLocalRef(t.classID);
		}
	}
	void CCActionSheet(const char * pszTitle, IndexedDialogDelegateRef delegate, const char * pszCancelBtnTitle, const char * destructiveButtonTitle, const std::vector<std::string>& otherButtonTitles)
	{

		JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(
										   t,
										   CLASS_NAME,
										   RAD_JAVA_ACTIONSHEET_FUNCNAME,
										   NDK_ARG_TYPES NDK_STRING_TYPE NDK_STRING_TYPE NDK_STRING_TYPE NDK_ARRAYLIST_TYPE NDK_RETURN_TYPE NDK_VOID_TYPE
										   ))
		{

			jstring title, cancelButton, confirmButton;
			jobject otherButtons;
			
			pushDelegate(delegate);
			
			title = createJString(t.env, pszTitle);
			cancelButton = createJString(t.env,pszCancelBtnTitle);
			confirmButton = createJString(t.env, destructiveButtonTitle);
			otherButtons = convertVectorToArrayList(t.env, otherButtonTitles);
			LOGD("create arraylist, now calling");
			
			t.env->CallStaticVoidMethod(t.classID, t.methodID, NDK_ACTIONSHEET_CALL_MARSHALLING(title, cancelButton, confirmButton, otherButtons));
			
			LOGD("called");
			t.env->DeleteLocalRef(title);
			t.env->DeleteLocalRef(cancelButton);
			t.env->DeleteLocalRef(confirmButton);
			t.env->DeleteLocalRef(otherButtons);
			t.env->DeleteLocalRef(t.classID);
		}
	}
}

extern "C" {

	JNIEXPORT void JNICALL Java_net_rapidappdevel_DialogHelper_onDialogDismissed(JNIEnv*  env, jobject thiz, int which, jstring t1, jstring t2)
	{
		
		auto delegate = getCurrentDelegate();
		LOGD("onDialogDismissed:: popping");
		popDelegate();
		LOGD("onDialogDismissed:: popped");
		
		if (delegate) {
			std::string s1, s2;
			LOGD("onDialogDismissed:: del");
			if (t1)
				s1 = JniHelper::jstring2string(t1);
			LOGD("onDialogDismissed:: s1 = %s",s1.c_str());
			if (t2)
			{
				s2 = JniHelper::jstring2string(t2);
				LOGD("onDialogDismissed:: s2 = %s",s2.c_str());
			}
			LOGD("onDialogDismissed:: calling del");
				// The name and pass
			delegate->clickedButtonAtIndex(which, s1.c_str(), s2.c_str());
			LOGD("onDialogDismissed:: called");
		}

	}
}
