//
//  Application_extensions.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/10/14.
//
//

#include "../Application_extensions.h"
#include "details/NDKHelperObjects.h"
#include <cstdlib>
#include <jni.h>
#include <android/log.h>
#include "jni/JniHelper.h"
#include "FileSystem.h"
#include <sstream>

#define CLASS_NAME "net/rapidappdevel/AppExtensionHelper"
#define  LOG_TAG    "Java_net_rapidappdevel_AppExtensionHelper.cpp"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

using namespace cocos2d;

static jstring createJString(JNIEnv*  env, const std::string& str) {
		if (str.empty()) {
			return env->NewStringUTF("");
		} else {
			return env->NewStringUTF(str.c_str());
		}
	}
namespace
{
	template <typename T>
	std::string to_string(const T& t) {
		std::ostringstream os;
		os << t;
		return os.str();
	}
//---------------------------------------------------------------------------
	std::string fileSansDirectory(const std::string& path){
		std::string::size_type index = path.rfind('/');
		return path.substr(index+1);
	}

	std::string extRemove(const std::string& file){
		std::string::size_type index = file.rfind('.');
		return file.substr(0,index);
	}
	std::string extOnly(const std::string& file){
		std::string::size_type index = file.rfind('.');
		return file.substr(index);
	}

	std::string generateUniqueDestinationPathFromName(const std::string &destPath, const std::string &file){
		std::string newDestPath = destPath, newFile = file;
		int i = 1;
		if(destPath.rfind('/')!=destPath.length()-1){
			newDestPath += "/";
		}
		while (rad::fs::exists(newDestPath + newFile)) {
			std::string fileName = extRemove(file), fileExt = extOnly(file);
			newFile = fileName + "-" + to_string(i) + fileExt;
			i++;
		}
		return std::move(newDestPath+newFile);
	}
//---------------------------------------------------------------------------
}
namespace rad { namespace Application_extensions {
	
	bool openURL(const std::string& url)
	{
		bool result = false;
		JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(
										   t,
										   CLASS_NAME,
										   "openURL",
										   NDK_ARG_TYPES NDK_STRING_TYPE NDK_RETURN_TYPE NDK_BOOL_TYPE
										   )) {
			jstring title;
			title = createJString(t.env, url);
			result = t.env->CallStaticBooleanMethod(t.classID, t.methodID, title);

			t.env->DeleteLocalRef(title);
			t.env->DeleteLocalRef(t.classID);
		}
		return result;
	}
	bool canOpenURL(const std::string& url)
	{
		bool result = false;
		JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(
										   t,
										   CLASS_NAME,
										   "canOpenURL",
										   NDK_ARG_TYPES NDK_STRING_TYPE NDK_RETURN_TYPE NDK_BOOL_TYPE
										   )) {
			jstring title;
			title = createJString(t.env, url);
			result = t.env->CallStaticBooleanMethod(t.classID, t.methodID, title);

			t.env->DeleteLocalRef(title);
			t.env->DeleteLocalRef(t.classID);
		}
		return result;
	}
	//-----------------------------------------------------------------------------------------------------------------------------
	void shareURL(const std::string& url)
	{
		//generateUniqueDestinationPathFromName(url)
		//rename to generateUniqueDestinationPathFromName(url)
		
		
		const std::string dest_directory("/storage/sdcard0/DCIM/VidXtream/");
		
		if(!(rad::fs::exists(dest_directory)))
			rad::fs::create_directories(dest_directory);
		
		//temp =  dest_directory + filename
		std::string temp = generateUniqueDestinationPathFromName(dest_directory, fileSansDirectory(url));
		
		rad::fs::rename(url,temp);
			
				
		

		
	//-----------------------------------------------------------------------------------------------------------------------------
		JniMethodInfo t;
		if (JniHelper::getStaticMethodInfo(
										   t,
										   CLASS_NAME,
										   "shareURL",
										   NDK_ARG_TYPES NDK_STRING_TYPE NDK_RETURN_TYPE NDK_VOID_TYPE
										   )) {
			jstring title;
			title = createJString(t.env, temp);
			t.env->CallStaticVoidMethod(t.classID, t.methodID, title);

			t.env->DeleteLocalRef(title);
			t.env->DeleteLocalRef(t.classID);
		}
	}
	std::string getCacheDirectory()
	{
		return nullptr;
	}
	
} }

extern "C" {
	
	JNIEXPORT void JNICALL Java_net_rapidappdevel_AppExtensionHelper_onHardwareBackButtonPressed(JNIEnv*  env, jobject thiz)
	{
		LOGD("onHardwareBackButtonPressed");
	}
}
