//
//  NativeVideoPlayer_Android.cpp
//  POV2
//
//  Created by Clayton Thomas on 6/9/14.
//
//

#include "../NativeVideoPlayer.h"
#include "cocos2d.h"
#include "details/NDKHelperObjects.h"
#include <cstdlib>
#include <jni.h>
#include "jni/JniHelper.h"

#define CLASS_NAME "net/rapidappdevel/AppExtensionHelper"

using namespace cocos2d;

namespace {
	struct Privy {
		std::string path;
		std::function<void (const std::string&)> completion;
	};

	Privy pPlaying;

	void videoDidFinishPlaying() {
		auto& playing = pPlaying;
		if (playing.completion)
			playing.completion(playing.path);
		cocos2d::CCNotificationCenter::sharedNotificationCenter()->postNotification(kNativeVideoPlayerDidFinishNotification, cocos2d::CCString::create(playing.path));
		playing.path.clear();
		playing.completion = nullptr;
	}

	jstring createJString(JNIEnv*  env, const std::string& str) {
		if (str.empty()) {
			return env->NewStringUTF("");
		} else {
			return env->NewStringUTF(str.c_str());
		}
	}
}

namespace rad { namespace NativeVideoPlayer {
	
	const char * kDidFinishNotification = "kNativeVideoPlayerDidFinishNotification";
	
	void playVideo(const std::string& path, std::function<void (const std::string&)> completion)
	{
		auto& playing = pPlaying;

		playing.path = path;
		playing.completion = completion;

		JniMethodInfo t;
		if(JniHelper::getStaticMethodInfo(
											t,
											CLASS_NAME,
											"playVideo",
											NDK_ARG_TYPES NDK_STRING_TYPE NDK_RETURN_TYPE NDK_VOID_TYPE
											)){
			jstring jPath;
			jPath = createJString(t.env, path);
			t.env->CallStaticVoidMethod(t.classID, t.methodID, jPath);

			t.env->DeleteLocalRef(jPath);
			t.env->DeleteLocalRef(t.classID);
		}
	}
	
} }

extern "C" {

	JNIEXPORT void JNICALL Java_net_rapidappdevel_AppExtensionHelper_onVideoFinished(JNIEnv* env, jobject thiz)
	{
		videoDidFinishPlaying();
	}
}
