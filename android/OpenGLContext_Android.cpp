//
//  OpenGLContext.cpp
//  MacH264InDisplay
//
//  Created by Clayton Thomas on 5/2/14.
//
//

#include "../OpenGLContext.h"
#include "CCGL.h"
#include <EGL/egl.h>
#include <EGL/eglplatform.h>
#include <memory>

typedef void GLContextType;


using namespace rad;

namespace
{
	struct _CCContext {
		EGLContext context;
		EGLDisplay display;
		_CCContext()
		: context(EGL_NO_CONTEXT)
		, display(eglGetDisplay(EGL_DEFAULT_DISPLAY))
		{
			context = eglGetCurrentContext();
			fprintf(stderr, "Current context = %p\n", context);
		}
		EGLContext getContext() {
			if (context == EGL_NO_CONTEXT)
				context = eglGetCurrentContext();
			fprintf(stderr, "Current context = %p\n", context);
			return context;
		}
	};
	_CCContext _context;

	EGLConfig configForDefaultDisplay() {
		/* Get a display handle and initalize EGL */
		EGLint major, minor;
		EGLDisplay display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		eglInitialize(display, &major, &minor);
		 
		/* Request an RGB565 config. with 4x anti-aliasing
		* (4x is 'free' on Mali :-)
		*/
		EGLint attributes[] = { EGL_RED_SIZE, 5, EGL_GREEN_SIZE, 6, EGL_BLUE_SIZE, 5, EGL_SAMPLES, 4, EGL_NONE };
		EGLint numberConfigs;
		EGLConfig* matchingConfigs;

		// /* Number of matching EGLConfig's returned in numberConfigs, but because
		// * the 3rd argument is NULL no configs will actually be returned
		// */
		// if (EGL_FALSE == eglChooseConfig(display, attributes, NULL,0, &numberConfigs)) 
		// {
		// /* An error */
		// 	fprintf(stderr, "%s\n", "Failed to eglChooseConfig");
		// 	return;
		// }
		// if (numberConfigs == 0) {
		// /* An error */
		// 	fprintf(stderr, "%s\n", "Failed to eglChooseConfig because numberConfigs == 0");
		// 	return;
		// }
		 
		/* Allocate some space to store list of matching configs... */
		matchingConfigs = (EGLConfig*)malloc( numberConfigs * sizeof(EGLConfig));
		/* The EGLConfig we will eventually use.
		* Set to NULL to detect the case where no suitable config. Is found
		*/
		EGLConfig chosenConfig = nullptr;

		/* ...and this time actually get the list (notice the 3rd argument is
		* now the buffer to write matching EGLConfig's to)
		*/
		if (EGL_FALSE == eglChooseConfig(display, attributes, matchingConfigs, numberConfigs, &numberConfigs)) 
		{
		/* An error */
			fprintf(stderr, "%s\n", "Failed to eglChooseConfig");
		}
		else
		{
			/* Look at each EGLConfig */
			for (int ii=0; ii<numberConfigs; ++ii) 
			{
			    EGLBoolean success;
			    EGLint red, green, blue;
			    /* Read the relevent attributes of the EGLConfig */
			    success = eglGetConfigAttrib(display, matchingConfigs[ii],
			    EGL_RED_SIZE, &red);
			    success &= eglGetConfigAttrib(display, matchingConfigs[ii],
			    EGL_BLUE_SIZE, &blue);
			    success &= eglGetConfigAttrib(display, matchingConfigs[ii],
			    EGL_GREEN_SIZE, &green);
			 
			    /* Check that no error occurred and the attributes match */
			    if ( (success == EGL_TRUE) && (red==5) && (green==6) && (blue==5) ) 
			    {
			    	chosenConfig = matchingConfigs[ii];
			    	break;
			    }
			}
			 
			if (chosenConfig == nullptr) 
			{
			    /* An error */
			    fprintf(stderr, "%s\n", "Failed chosenConfig == NULL");
			}
		}
		 
		free(matchingConfigs);
		return chosenConfig;
	}

	GLContextType* CreateContext(const EGLint* contextAttribs, EGLConfig config, GLContextType * shared_context = EGL_NO_CONTEXT) {
		
		return eglCreateContext(eglGetDisplay(EGL_DEFAULT_DISPLAY), config, shared_context, &contextAttribs[0]);
	}
	
	GLContextType *GetCocos2dContext() {
		return _context.getContext();
	}

}

class OpenGLContext::Impl
{
	GLContextType *context;
	EGLSurface surface;
	EGLDisplay display;

public:

	~Impl()
	{
		freeContext();
	}
	Impl()
	: context(EGL_NO_CONTEXT)
	, surface(EGL_NO_SURFACE)
	, display(EGL_NO_DISPLAY)
	{
		
	}
	
	bool init(OpenGLContext* shared) {
		return this->init(shared->impl->context);
		// context = CreateContext(shared->impl->context);
		// return context != nullptr;
	}
	
	bool init(GLContextType* shared) {
		const EGLint contextAttribs[] = {EGL_CONTEXT_CLIENT_VERSION,2,EGL_NONE};
		EGLConfig config = configForDefaultDisplay();
		this->display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		this->surface = eglCreateWindowSurface(this->display, config, nullptr, nullptr);
		context = CreateContext(&contextAttribs[0], config, shared);

		return context != EGL_NO_CONTEXT;
	}
	
	bool isCurrent() const {
		return context == eglGetCurrentContext();
	}
	
	bool makeCurrent() {
		auto ret = false;
			
		if (false == isCurrent()) {
			glFlush();
			glBindTexture(GL_TEXTURE_2D, 0);
			setCurrentContext(this->context, this->display, this->surface);
			ret = true;
		}
		
		return ret;
	}
	
private:
	void freeContext() {
		if (context) {
			if (true == isCurrent())
				setCurrentContext(EGL_NO_CONTEXT);
			eglDestroySurface(this->display, this->surface);
			eglDestroyContext(this->display, this->surface);
		}
		context = EGL_NO_CONTEXT;
		display = EGL_NO_DISPLAY;
		surface = EGL_NO_SURFACE;
	}
	
public:
	static
	void setCurrentContext(GLContextType *c, EGLDisplay display = EGL_NO_DISPLAY, EGLSurface surface = EGL_NO_SURFACE) {
		eglMakeCurrent(display, surface, surface, c);
	}
};


OpenGLContext::~OpenGLContext()
{

}

OpenGLContext::OpenGLContext()
: impl(new OpenGLContext::Impl())
{
	
}

bool OpenGLContext::init()
{
	return initWithSharedContext(nullptr);
}
bool OpenGLContext::initWithSharedContext(OpenGLContext *shared)
{
	return impl->init(shared);
}

bool OpenGLContext::initWithCocos2dAsSharedContext()
{
	return impl->init(GetCocos2dContext());
}
bool OpenGLContext::isCurrent() const
{
	return impl->isCurrent();
}
bool OpenGLContext::makeCurrent()
{
	return impl->makeCurrent();
}
bool OpenGLContext::setCurrentContext(OpenGLContext* context)
{
	bool ret = true;
	if (context == nullptr)
	{
		OpenGLContext::Impl::setCurrentContext(nullptr);
	}
	else {
		ret = context->makeCurrent();
	}
	return ret;
}
